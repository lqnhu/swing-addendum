package net.asmcbain.swing.components.sheet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FocusTraversalPolicy;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.UUID;
import java.util.concurrent.SynchronousQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import net.asmcbain.swing.layout.SingleCenteredItemLayout;

/**
 * Based on the idea of a "Sheet" from the Mac operating system. Credits go to the Swing
 * Hacks book for a start on the code.
 * 
 * @author Art McBain
 *
 */
public class MotionAnimatedSheetGlassPane extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(MotionAnimatedSheetGlassPane.class.getName());

	private final SynchronousQueue<Object> synchq;
	private JOptionPane optionPane;
	private JComponent sheet;
	private final JPanel dialogLayer;
	private final JPanel dialogShadowLayer;
	private final DropShadowPanel dropShadowPanel;
	private final FocusTraversalPolicy limitedPolicy;

	private FocusTraversalPolicy previousPolicy;
	private BufferedImage sheetImage;
	private Object value;

	private boolean isDialogShowing;

	private Runnable callback;

	private static final int ANIMATION_SLEEP = 10;
	private static final double ANIMATION_DURATION = 500f;
	private static final int IN = 1;
	private static final int OUT = -1;

	private int animationDirection;
	private long animationStartTime;
	private Timer animation = new Timer(ANIMATION_SLEEP, new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			if(sheetImage == null) animation.stop();

			double animationPercent = Math.min(1d, (System.currentTimeMillis() - animationStartTime) / ANIMATION_DURATION);

			int height = 0;
			if(animationDirection == IN) {
				height = (int)(animationPercent * sheetImage.getHeight());
			} else {
				height = (int)((1d - animationPercent) * sheetImage.getHeight());
			}

			if(height > 0) {
				BufferedImage image = sheetImage.getSubimage(0, sheetImage.getHeight() - height, sheetImage.getWidth(), height);
				dropShadowPanel.setSubject(image);
				Point offset = new Point(0, image.getHeight() - dropShadowPanel.getPreferredSize().height + dropShadowPanel.getShadowSize() * 2);
				((SingleCenteredItemLayout)dialogShadowLayer.getLayout()).setOffset(dialogShadowLayer, offset);
				dialogShadowLayer.revalidate();
			}

			Container container = MotionAnimatedSheetGlassPane.this.getParent();
			if(container != null) {
				container.repaint();
			}

			if(animationPercent >= 1d) {
				animation.stop();

				if(animationDirection == OUT) {
					hideSheet();
					try {
						synchq.put(value);
						isDialogShowing = false;
					} catch(InterruptedException err) {
						LOGGER.log(Level.WARNING, "failed to put dialog return value", err); // damn
					}
				} else {
					dialogLayer.add(sheet, SingleCenteredItemLayout.CENTER_NORTH);
					sheet.requestFocusInWindow();
					if(callback != null) {
						callback.run();
					}
				}
			}
		}		
	});

	/**
	 * Construct a default SheetGlassPane
	 */
	public MotionAnimatedSheetGlassPane() {
		super(new StackLayout());

		synchq = new SynchronousQueue<Object>();

		setOpaque(false);
		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// block
			}
			public void mousePressed(MouseEvent e) {
				// block
			}
			public void mouseReleased(MouseEvent e) {
				// block
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				// block
			}
			public void mouseMoved(MouseEvent e) {
				// block
			}
		});

		// Allows some focus movement, via tab and such. Checks each component which would
		// be returned to see if it has the sheet as an ancestor. Seems to work. As a side
		// note, if null is returned from all methods, no focus traversal is allowed.
		limitedPolicy = new FocusTraversalPolicy() {

			private boolean isOutsideSheet(Component component) {
				if(component == null) return true;
				return SwingUtilities.getAncestorNamed(sheet.getName(), component) == null;
			}

			public Component getComponentAfter(Container container, Component component) {
				if(container == null || component == null) return null;
				Component next = previousPolicy.getComponentAfter(container, component);
				return (!isOutsideSheet(next))? next : getFirstComponent(container);
			}

			public Component getComponentBefore(Container container, Component component) {
				if(container == null || component == null) return null;
				Component before = previousPolicy.getComponentBefore(container, component);
				return (!isOutsideSheet(before))? before : getLastComponent(container);
			}

			public Component getDefaultComponent(Container container) {
				return container;
			}

			public Component getFirstComponent(Container container) {
				return previousPolicy.getFirstComponent(sheet);
			}

			public Component getLastComponent(Container container) {
				return previousPolicy.getLastComponent(sheet);
			}
			
		};

		dialogLayer = new JPanel(new SingleCenteredItemLayout());
		dialogLayer.setOpaque(false);
		add(dialogLayer, StackLayout.TOP);

		dropShadowPanel = new DropShadowPanel();
		dropShadowPanel.setAngle(90f);

		dialogShadowLayer = new JPanel(new SingleCenteredItemLayout());
		dialogShadowLayer.setOpaque(false);
		dialogShadowLayer.add(dropShadowPanel, SingleCenteredItemLayout.CENTER_NORTH);
		add(dialogShadowLayer, StackLayout.BOTTOM);
	}

	public boolean isOptimizedDrawingEnabled() {
		return false;
	}

	/**
	 * Shows a dialog with the given data, message types, and option type.
	 * 
	 * @see JOptionPane
	 * @param data to show as the dialog's message
	 * @param messageType of the dialog
	 * @param optionType of the dialog
	 * @return the user-chosen value resulting from this dialog
	 */
	public Object showDialog(Object data, int messageType, int optionType) {
		return showDialog(data, messageType, optionType, null);
	}

	/**
	 * Shows a dialog with the given data, message types, option type, and a callback
	 * to notify when this dialog is entirely visible.
	 * 
	 * @see JOptionPane
	 * @param data to show as the dialog's message
	 * @param messageType of the dialog
	 * @param optionType of the dialog
	 * @param callback to be run when the dialog is completely shown
	 * @return the user-chosen value resulting from this dialog
	 */
	public Object showDialog(Object data, int messageType, int optionType, Runnable callback) {
		this.callback = callback;

		optionPane = new JOptionPane(data, messageType, optionType);
		optionPane.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				if(evt.getPropertyName().equals(JOptionPane.VALUE_PROPERTY)) {

					Graphics g = sheetImage.getGraphics();
					sheet.paint(g);

					MotionAnimatedSheetGlassPane.this.getTopLevelAncestor().setFocusTraversalPolicy(previousPolicy);
					dialogLayer.remove(sheet);
					value = evt.getNewValue();

					animationDirection = OUT;
					animationStartTime = System.currentTimeMillis();
					animation.start();
				}
			}
		});

		JDialog dialog = optionPane.createDialog(getTopLevelAncestor(), "The meaning of life is ...");
		sheet = (JComponent)dialog.getContentPane();
		sheet.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		sheet.setName(UUID.randomUUID().toString());

		animationDirection = IN;
		animationStartTime = System.currentTimeMillis();
		sheetImage = new BufferedImage(sheet.getPreferredSize().width, sheet.getPreferredSize().height, BufferedImage.TYPE_INT_RGB);
		Graphics g = sheetImage.getGraphics();
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, sheetImage.getWidth(), sheetImage.getHeight());
		g.translate(1, 1);
		sheet.paint(g);
		animation.start();

		previousPolicy = getTopLevelAncestor().getFocusTraversalPolicy();
		getTopLevelAncestor().setFocusTraversalPolicy(limitedPolicy);

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				setVisible(true);				
			}
		});

		isDialogShowing = true;
		try {
			return synchq.take();
		} catch(InterruptedException e) {
			hideSheet();
			return optionPane.getValue();
		}
	}

	/**
	 * Hides the currently showing dialog
	 */
	private void hideSheet() {
		setVisible(false);
	}

	/**
	 * Whether or not a dialog is currently being shown.
	 * 
	 * @return <code>true</code> if a dialog is visible
	 */
	public boolean isDialogShowing() {
		return isDialogShowing;
	}

}

