package net.asmcbain.swing.components.table;

import javax.swing.table.TableModel;

/**
 * Indicates a TableModel that is mutable in the sense of being
 * able to add or remove rows.
 * 
 * @author Art McBain
 *
 */
public interface MutableTableModel extends TableModel {

	/**
	 * Adds a column to the model. All listeners should receive a tableChanged notice.
	 * 
	 * @param name of the new column
	 */
	public void addColumn(Object name);

	/**
	 * Adds a row to the end of the model. All listeners should be notified of the row
	 * addition.
	 * 
	 * @param data of the new row
	 */
	public void addRow(Object[] data);

	/**
	 * All listeners should be notified that the range from the first indicated row
	 * to the last (inclusive) have updated.
	 * 
	 * @param firstRow for which to notify updates
	 * @param lastRow for which to notify updates
	 */
	public void fireTableRowsUpdated(int firstRow, int lastRow);

	/**
	 * Removes the row at row from the model. All listeners should be notified of the
	 * row removal.
	 * 
	 * @param row to be removed
	 * @throws Exception if the row is outside of the range of the MutabletableModel 
	 */
	public void removeRow(int row) throws IndexOutOfBoundsException;

	/**
	 * Replaces the column identifiers in the model.
	 * 
	 * @param columnIdentifiers names of the columns
	 */
	public void setColumnIdentifiers(Object[] columnIdentifiers);

	/**
	 * Sets the number of columns in the model. If the column count is larger than the
	 * current number of columns, new default data should be added. Similarly if the
	 * new column count is smaller, data at index columnCount and greater should be
	 * discarded.
	 * 
	 * @param count number of columns in the model
	 */
	public void setColumnCount(int count);

	/**
	 * Sets the number of rows in the model. If the new size is greater than the current
	 * size, new rows should added to the end. If the new size is smaller than the
	 * current size, all rows at index rowCount and greater should be discarded.
	 * 
	 * @param count number of rows in the model
	 */
	public void setRowCount(int count);

}
