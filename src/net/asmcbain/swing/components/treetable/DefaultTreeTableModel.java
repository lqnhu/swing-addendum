package net.asmcbain.swing.components.treetable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import net.asmcbain.swing.components.table.MutableTableModel;

/**
 * A rather simplistic TreeTableModel. It manages "open" nodes folders, and table updates.
 * <br><br>
 * This TreeTableModel is "flat", meaning it handles proper ordering/nesting of items, but
 * but it doesn't have any indication as to indentation, that was deferred to the
 * TreeTableCellRenderer.
 * 
 * @author Art McBain
 *
 */
public class DefaultTreeTableModel extends AbstractTableModel implements MutableTableModel, TreeTableModel, MutableTreeTableModel {
	private static final long serialVersionUID = 594489916628545212L;

	protected final List<Object> columns;
	protected final List<TreeTableNode<?>> rows;
	protected final HashSet<TreeTableNode<?>> open;

	/**
	 * Creates a default DefaultTreeTableModel.
	 */
	public DefaultTreeTableModel() {
		open = new HashSet<TreeTableNode<?>>();
		rows = new ArrayList<TreeTableNode<?>>();
		columns = new ArrayList<Object>();
	}

	public void addColumn(Object columnName) {
		columns.add(columnName);
	}

	public void addNode(TreeTableNode<?> node) {
		rows.add(node);
		fireTableRowsInserted(rows.size() - 1, rows.size() - 1);
	}

	public void addRow(Object[] rowData) {
		if(rowData != null && rowData.length > 0 && rowData[0] instanceof TreeTableNode<?>) {
			addNode((TreeTableNode<?>)rowData[0]);
		}
	}

	public void closeNode(TreeTableNode<?> node) {
		if(open.contains(node)) {
			final Range r = recursiveCloseNode(node, true);
			fireTableRowsUpdated(r.index, r.index);
			fireTableRowsDeleted(r.index + 1, r.index + r.length);
		}
	}

	public int getColumnCount() {
		return (columns == null)? 0 : columns.size();
	}

	public TreeTableNode<?> getNodeAtRow(int row) {
		return (row < rows.size() && row >= 0)? rows.get(row) : null;
	}

	// Possibly called before this class is fully set up. Therefore: null check.
	public int getRowCount() {
		return (rows != null)? rows.size() : 0;
	}

	public Object getValueAt(int row, int column) {
		TreeTableNode<?> node = (row < rows.size())? rows.get(row) : null;
		return (node != null)? node.getColumnValue(column) : null;
	}

	public boolean isCellEditable(int column, int row) {
		return false;
	}

	public boolean isOpen(TreeTableNode<?> node) {
		synchronized(rows) {
			return open.contains(node);
		}
	}

	public void openNode(TreeTableNode<?> node) {
		synchronized(rows) {
			if(!open.contains(node) && node.hasChildren()) {
				final Range r = recursiveOpenNode(node);
				fireTableRowsUpdated(r.index, r.index);
				fireTableRowsInserted(r.index + 1, r.index + r.length);
			}
		}
	}

	public void removeNode(TreeTableNode<?> node) {
		removeRow(rows.indexOf(node));
	}

	public void removeRow(int row) {
		synchronized(rows) {
			if(row < 0 || row >= getRowCount()) {
				throw new ArrayIndexOutOfBoundsException("row to be removed must be within: 0 < row < model.getRowCount()");
			}
			rows.remove(row);
			fireTableRowsDeleted(row, row);
		}
	}

	public void setColumnCount(int count) {
		if(count < 0) return;

		int size = columns.size();
		for(; count < size; count++) {
			columns.remove(columns.size() - 1);
		}
	}

	public void setColumnIdentifiers(Object[] columnIdentifiers) {
		this.columns.clear();
		this.columns.addAll(Arrays.asList(columnIdentifiers));		
	}

	public void setColumnNames(Object... names) {
		setColumnIdentifiers(names);
	}

	// Given that this is a tree-table ... this may not do what you want
	public void setRowCount(int count) {
		if(count < 0) throw new IllegalArgumentException("rowCount must be >= 0");
		int size = rows.size();
		rows.removeAll(new ArrayList<TreeTableNode<?>>(rows.subList(0, size - count)));
		fireTableRowsDeleted(count, Math.max(0, size - 1));
	}

	/**
	 * Recursively "closes" nodes. It does not close open sub-nodes so if this node is
	 * reopened, those nodes will be as well.
	 * 
	 * @param node to be closed
	 * @param removeNode if node is to be removed from the open nodes list
	 * @return the range of indexes of nodes closed
	 */
	private Range recursiveCloseNode(TreeTableNode<?> node, boolean removeNode) {
		if(removeNode) {
			open.remove(node);
		}

		int index = rows.indexOf(node);
		int length = 0;

		Collection<? extends TreeTableNode<?>> list = node.getChildren();
		for(TreeTableNode<?> entry : list) {
			rows.remove(entry);

			if(open.contains(entry)) {
				Range r = recursiveCloseNode(entry, false);
				length += r.length;
			}
		}

		return new Range(index, list.size() + length);
	}

	/**
	 * Recursively opens a node. This is in case there are open nodes inside this node when
	 * it is (re)opened.
	 * 
	 * @param node to be opened
	 * @return the range of indexes of nodes opened
	 */
	private Range recursiveOpenNode(TreeTableNode<?> node) {
		open.add(node);

		int index = rows.indexOf(node);
		int length = 0;

		Collection<? extends TreeTableNode<?>> list = node.getChildren();
		rows.addAll(index + 1, list);

		for(TreeTableNode<?> entry : list) {
			if(open.contains(entry)) {
				Range r = recursiveOpenNode(entry);
				length += r.length;
			}
		}

		return new Range(index, list.size() + length);
	}

	/**
	 * A simple class to manage a range of rows to tell the DefaultTableModel to update.
	 * 
	 * @author Art McBain
	 *
	 */
	protected static class Range {
		protected final int index;
		protected final int length;

		/**
		 * Creates a range with the given start and length.
		 * 
		 * @param n range start
		 * @param x range length
		 */
		public Range(int n, int x) {
			index = n;
			length = x;
		}

		public String toString() {
			return "Range(index=" + index + ", length=" + length + ")";
		}

	}

}
