package net.asmcbain.swing.components.treetable;

/**
 * A rather simplistic TreeTableModel.
 * 
 * @author Art McBain
 *
 */
public interface TreeTableModel {

	/**
	 * Close the given node, if it is open, and is managed by this model.
	 * 
	 * @param node to be closed
	 */
	public void closeNode(TreeTableNode<?> node);

	/**
	 * Used to get the node at a specific row.
	 * 
	 * @param row of the node to be returned
	 * @return a TreeTableNode
	 */
	public TreeTableNode<?> getNodeAtRow(int row);

	/**
	 * Returns whether the given node is open or not.
	 * 
	 * @param node to be checked
	 * @return a boolean
	 */
	public boolean isOpen(TreeTableNode<?> node);

	/**
	 * Opens the given node if it is not already open and if it is managed by this model.
	 * 
	 * @param node to be opened
	 */
	public void openNode(TreeTableNode<?> node);

}

