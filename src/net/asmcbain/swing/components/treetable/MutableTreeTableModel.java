package net.asmcbain.swing.components.treetable;

/**
 * Indicates a mutable TreeTableModel
 * 
 * @author Art McBain
 *
 */
public interface MutableTreeTableModel extends TreeTableModel {

	/**
	 * Adds the given node to the end of the model.
	 * 
	 * @param node to be added
	 */
	public void addNode(TreeTableNode<?> node);

	/**
	 * Removes the given node from the model.
	 * 
	 * @param node to be removed
	 */
	public void removeNode(TreeTableNode<?> node);

	/**
	 * Sets the column names for this model.
	 * 
	 * @param names of the columns
	 */
	public void setColumnNames(Object... names);

}
