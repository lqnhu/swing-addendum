package net.asmcbain.swing.components.treetable;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.plaf.metal.MetalIconFactory;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * A table cell that shows control icons for openable nodes as well as icons for leaves. This
 * implementation is relatively simple, in that it ignores TreeTableNode.getType(), and as
 * such, leaves are just nodes with no children. Indentation is by asking each node it's
 * depth from the root node. The further it is from the root node, the more the indentation.
 * 
 * @author Art McBain
 *
 */
public class TreeTableCellRenderer extends JPanel implements TableCellRenderer {
	private static final long serialVersionUID = -8526572829387040526L;

	private final Dimension dimension;
	private final DefaultTableCellRenderer label;
	private final JPanel spacer;
	private final JLabel icon;

	private final TreeTableModel model;
	private final boolean showLeafIcons;

	private final Icon folder;
	private final Icon closed;
	private final Icon open;
	private final Icon leaf;
	private final Icon empty;

	/**
	 * Creates a TreeTableCellRenderer with the given TreeTabelModel, and whether or not it
	 * should show leaf icons.
	 * 
	 * @param model associated with the table using this renderer
	 */
	public TreeTableCellRenderer(TreeTableModel model) {
		this(model, false);
	}

	/**
	 * Creates a TreeTableCellRenderer with the given TreeTabelModel, and whether or not it
	 * should show leaf icons.
	 * 
	 * @param model associated with the table using this renderer
	 * @param showLeafIcons whether to show icons for leaf nodes
	 */
	public TreeTableCellRenderer(TreeTableModel model, boolean showLeafIcons) {
		this.model = model;
		this.showLeafIcons = showLeafIcons;

		setOpaque(true);
		setLayout(new BorderLayout());
		dimension = new Dimension(0, 0);

		add(spacer = new JPanel(), BorderLayout.WEST);

		JPanel center = new JPanel(new BorderLayout());
		center.add(icon = new JLabel(), BorderLayout.WEST);
		center.add(label = new DefaultTableCellRenderer(), BorderLayout.CENTER);
		add(center, BorderLayout.CENTER);
		icon.setOpaque(true);

		folder = MetalIconFactory.getTreeFolderIcon();
		closed = MetalIconFactory.getTreeControlIcon(true);
		open = MetalIconFactory.getTreeControlIcon(false);
		leaf = MetalIconFactory.getTreeLeafIcon();
		empty = new Icon() {

			public int getIconHeight() {
				return open.getIconHeight();
			}

			public int getIconWidth() {
				return open.getIconWidth();
			}

			public void paintIcon(Component c, Graphics g, int x, int y) {
				// do nothing
			}

		};
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		label.setIcon(null);
		icon.setIcon(null);

		if(value instanceof TreeTableNode<?>) {

			TreeTableNode<?> node = (TreeTableNode<?>)value;
			if(node.hasChildren()) {
				label.setIcon(folder);
				icon.setIcon((model.isOpen(node))? open : closed);
			} else {
				label.setIcon((showLeafIcons)? leaf : empty);
				icon.setIcon(empty);
			}
			value = node.getColumnValue(column);

			int indent = (node.getDepth() == 0)? 0 : (node.hasChildren())? 1 : 2;
			dimension.setSize((node.getDepth() + indent) * open.getIconWidth(), 0);
			spacer.setPreferredSize(dimension);
		} else {
			dimension.setSize(0, 0);
			spacer.setPreferredSize(dimension);
		}

		label.getTableCellRendererComponent(table, value, isSelected, false, row, column);
		icon.setBackground(label.getBackground());
		spacer.setBackground(label.getBackground());

		return (this);
	}

}
