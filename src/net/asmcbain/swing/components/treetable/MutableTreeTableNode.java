package net.asmcbain.swing.components.treetable;

public abstract class MutableTreeTableNode<T extends TreeTableNode<T>> extends TreeTableNode<T> {

	public MutableTreeTableNode(T parent) {
		super(parent);
	}

	/**
	 * Adds the given node as a child.
	 * 
	 * @param node to be added as a child
	 * @return <code>true</code> if the addition succeeds
	 */
	public abstract boolean addChild(T node);

	/**
	 * Inserts the given node after the indicated "current" node. If the
	 * indicated current node is not a child of the node on which this method
	 * is called, no action should be done and false returned.
	 * 
	 * @param current the reference "current" node
	 * @param node the node to insert after the "current" node
	 * @return <code>true</code> if the insertion succeeds
	 */
	public abstract boolean insertChildAfter(T current, T node);

	/**
	 * Inserts the given node before the indicated "current" node. If the
	 * indicated current node is not a child of the node on which this method
	 * is called, no action should be done and false returned. 
	 * 
	 * @param current the reference "current" node
	 * @param node the node to insert before the "current" node
	 * @return <code>true</code> if the insertion succeeds
	 */
	public abstract boolean insertChildBefore(T current, T node);

	/**
	 * Removes the given node, if it is a child of the node on which this is
	 * called.
	 * 
	 * @param node child node to be removed
	 * @return <code>true</code> if the removal succeeds
	 */
	public abstract boolean removeChild(T node);

}
