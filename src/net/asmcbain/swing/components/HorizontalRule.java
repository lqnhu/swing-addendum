package net.asmcbain.swing.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * A horizontal-rule class, similar to HTML's &lt;hr/&gt;.
 * This class will work best inside of a BorderLayout.
 * 
 * @author Art McBain
 *
 */
public class HorizontalRule extends JPanel {
	private static final long serialVersionUID = 1L;

	/**
	 * A default horizontal-rule with no padding.
	 */
	public HorizontalRule() {
		this(10, 0);
	}

	/**
	 * A solid color horizontal-rule with no padding.
	 */
	public HorizontalRule(Color color) {
		this(10, 0, color);
	}


	/**
	 * A horizontal-rule with the same padding for top, left, bottom, and right.
	 * 
	 * @param padding the empty space around each side of the HorizontalRule
	 */
	public HorizontalRule(int padding) {
		this(padding, padding);
	}

	/**
	 * A solid color horizontal-rule with the same padding for top, left, bottom, and right.
	 * 
	 * @param padding the empty space around each side of the HorizontalRule 
	 * @param color of the HorizontalRule
	 */
	public HorizontalRule(int padding, Color color) {
		this(padding, padding, color);
	}

	/**
	 * A horizontal-rule with specified padding for left and right, top and bottom.
	 * 
	 * @param leftRight padding for the left and right sides
	 * @param topBottom padding for the top and bottom sides
	 */
	public HorizontalRule(int leftRight, int topBottom) {
		this(topBottom, leftRight, topBottom, leftRight);
	}

	/**
	 * A solid color horizontal-rule with specified padding for left and right, top and bottom.
	 * 
	 * @param leftRight padding for the left and right sides
	 * @param topBottom padding for the top and bottom sides
	 * @param color of the HorizontalRule
	 */
	public HorizontalRule(int leftRight, int topBottom, Color color) {
		this(topBottom, leftRight, topBottom, leftRight, color);
	}

	/**
	 * A horizontal-rule with specified padding for top, left, bottom, and right.
	 * 
	 * @param top padding
	 * @param left padding
	 * @param bottom padding
	 * @param right padding
	 */
	public HorizontalRule(int top, int left, int bottom, int right) {
		super(new BorderLayout());
		setPreferredSize(new Dimension(1, top + bottom + 2));

		JPanel north = new JPanel();
		north.setPreferredSize(new Dimension(1, top));
		add(north, BorderLayout.NORTH);

		JPanel center = new JPanel();
		center.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(0, left, 0, right), BorderFactory.createEtchedBorder()));
		center.setPreferredSize(new Dimension(1, 2));
		add(center, BorderLayout.CENTER);

		JPanel south = new JPanel();
		south.setPreferredSize(new Dimension(1, bottom));
		add(south, BorderLayout.SOUTH);
	}

	/**
	 * A solid color horizontal-rule with specified padding for top, left, bottom, and right.
	 * 
	 * @param top padding
	 * @param left padding
	 * @param bottom padding
	 * @param right padding
	 * @param color of the HorizontalRule
	 */
	public HorizontalRule(int top, int left, int bottom, int right, final Color color) {
		super(new BorderLayout());
		setPreferredSize(new Dimension(1, top + bottom + 2));

		JPanel north = new JPanel();
		north.setPreferredSize(new Dimension(1, top));
		add(north, BorderLayout.NORTH);

		JPanel center = new JPanel() {
			private static final long serialVersionUID = 1L;
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(color);
				g.drawLine(0, 0, getWidth(), 0);
			}
		};
		center.setPreferredSize(new Dimension(1, 1));
		add(center, BorderLayout.CENTER);

		JPanel south = new JPanel();
		south.setPreferredSize(new Dimension(1, bottom));
		add(south, BorderLayout.SOUTH);
	}

}

