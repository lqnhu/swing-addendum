package net.asmcbain.swing.components;

import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 * As the name implies, this JLabel has convenience methods for making some operations easier.
 * 
 * @author Art McBain
 * 
 */
public class ConvenientJList extends JList {
	private static final long serialVersionUID = 1L;

	private final DefaultListModel model;

	/**
	 * Creates a default ConvenienceJList.
	 */
	public ConvenientJList() {
		setModel(model = new DefaultListModel());
	}

	/**
	 * A convenience method for ((DefaultListModel)getModel()).addElement(obj);
	 * 
	 * @param obj to be added
	 */
	public void addItem(final Object obj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				model.addElement(obj);
			}
		});
	}

	/**
	 * A convenience method for ((DefaultListModel)getModel()).addSize(0);
	 */
	public void clearList() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				model.setSize(0);
			}
		});
	}

	/**
	 * Returns the size of this list.
	 * 
	 * @return the size of this list
	 */
	public int getListSize() {
		return model.getSize();
	}

	/**
	 * A convenience method for ((DefaultListModel)getModel()).removeElement(obj);
	 * 
	 * @param obj to be removed
	 */
	public void removeItem(final Object obj) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				model.removeElement(obj);
			}
		});
	}

}
