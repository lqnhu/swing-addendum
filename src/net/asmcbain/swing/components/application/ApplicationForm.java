package net.asmcbain.swing.components.application;

/**
 * Interface for all ApplicationForms.
 * 
 * @author Art McBain
 *
 */
public interface ApplicationForm {

	/**
	 * Focus the first field on the form (if possible).
	 */
	public void focusForm();

	/**
	 * Resets the form to the initial defaults.
	 */
	public void resetForm();

}
