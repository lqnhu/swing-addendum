package net.asmcbain.swing.components.application;

import java.awt.EventQueue;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.asmcbain.swing.components.sheet.MotionAnimatedSheetGlassPane;

/**
 * An "application window" that handles creating a default JFrame and setting up the
 * necessary elements so sheet-style dialogs can be shown on this window. 
 * 
 * @author Art McBain
 *
 */
public abstract class AbstractApplicationWindow {

	protected final JFrame frame;

	private boolean dialogAccepted;
	private final MotionAnimatedSheetGlassPane glass;

	private Thread dialogThread = new Thread(new Runnable() {
		public void run() {
			while(true) {
				while(!dialogQueue.isEmpty()) {
					Runnable item = dialogQueue.poll();
					if(item != null) item.run();
				}
				synchronized(dialogThread) {
					boolean error = false;
					do {
						try {
							dialogThread.wait();
							error = false;
						} catch(InterruptedException e) {
							error = true;
						}
					} while(error);
				}
			}
		}
	});
	private Queue<Runnable> dialogQueue = new LinkedList<Runnable>() {
		private static final long serialVersionUID = 1L;
		public boolean add(Runnable e) {
			boolean result = super.add(e);

			synchronized(dialogThread) {
				dialogThread.notifyAll();
			}

			return result;
		};
	};

	public AbstractApplicationWindow(String appName) {
		dialogThread.setDaemon(true);
		dialogThread.start();

		frame = new JFrame(appName);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setGlassPane(glass = new MotionAnimatedSheetGlassPane());
	}

	/**
	 * Sets the JMenuBar for this application window.
	 * 
	 * @param menubar to be used by this application window
	 */
	public void setJMenubar(final JMenuBar menubar) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				frame.setJMenuBar(menubar);	
			}
		});
	}

	/**
	 * Makes this window visible.
	 */
	public void show() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	/**
	 * Whether or not a dialog is currently showing on this ApplicationWindow.
	 * 
	 * @return <code>true</code> if a dialog is visible
	 * otherwise
	 */
	public boolean isDialogShowing() {
		return glass.isDialogShowing();
	}

	/**
	 * Whether or not the user accepted the last shown dialog
	 * with a yes/no choice.
	 * 
	 * @return <code>true</code> if the user accepted the last dialog
	 */
	public boolean wasDialogAccepted() {
		return dialogAccepted;
	}

	/**
	 * Shows an error dialog with the given text.
	 * 
	 * @param text to be shown
	 */
	public void showErrorDialog(final String text) {
		dialogQueue.add(new Runnable() {
			public void run() {
				glass.showDialog(text, JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION);
			}
		});
	}

	/**
	 * Shows an information dialog with the given text.
	 * 
	 * @param text to be shown
	 */
	public void showInformationDialog(final String text) {
		dialogQueue.add(new Runnable() {
			public void run() {
				glass.showDialog(text, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION);
			}
		});
	}

	/**
	 * Shows a plain dialog (no icon) with the given text.
	 * 
	 * @param text to be shown
	 */
	public void showPlainDialog(final String text) {
		dialogQueue.add(new Runnable() {
			public void run() {
				glass.showDialog(text, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
			}
		});
	}

	/**
	 * Shows a dialog asking the user the given question.
	 * 
	 * @param text to be shown
	 * @param callback to be called when the question dialog is closed
	 */
	public void showQuestionDialog(final String text, final Runnable callback) {
		if(callback == null) throw new IllegalArgumentException("Callback cannot be null!");
		dialogQueue.add(new Runnable() {
			public void run() {
				Object value = glass.showDialog(text, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
				if(value instanceof Integer) {
					int code = ((Integer)value).intValue();
					dialogAccepted = (code == JOptionPane.YES_OPTION);
					callback.run();
				} else {
					throw new ClassCastException("Type returned not an Integer");
				}
			}
		});
	}

	/**
	 * Shows a warning dialog with the given text.
	 * 
	 * @param text to be shown
	 */
	public void showWarningDialog(final String text) {
		dialogQueue.add(new Runnable() {
			public void run() {
				glass.showDialog(text, JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION);
			}
		});
	}

	/**
	 * Shows the user a okay dialog based on the given panel.
	 * 
	 * @param panel to be shown
	 */
	public void showPanelDialog(final JPanel panel) {
		dialogQueue.add(new Runnable() {
			public void run() {
				try {
					EventQueue.invokeAndWait(new Runnable() {
						public void run() {
							panel.revalidate();
						}
					});
				} catch(Exception e) {
					// It doesn't matter if this fails, really.
				}
				glass.showDialog(panel, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, new Runnable() {
					public void run() {
						if(panel instanceof ApplicationForm) {
							((ApplicationForm)panel).focusForm();
						}
					}
				});
			}
		});
	}

	/**
	 * Shows the user a okay/cancel dialog based on the given panel.
	 * 
	 * @param panel to be shown
	 * @param callback to be called when the dialog is closed
	 */
	public void showPanelDialog(final JPanel panel, final Runnable callback) {
		if(callback == null) throw new IllegalArgumentException("Callback cannot be null!");
		dialogQueue.add(new Runnable() {
			public void run() {

				try {
					EventQueue.invokeAndWait(new Runnable() {
						public void run() {
							panel.revalidate();
						}
					});
				} catch(Exception e) {
					// It doesn't matter if this fails, really.
				}
		
				Object value = glass.showDialog(panel, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, new Runnable() {
					public void run() {
						if(panel instanceof ApplicationForm) {
							((ApplicationForm)panel).focusForm();
						}
					}
				});
		
				if(value instanceof Integer) {
					int code = ((Integer)value).intValue();
					dialogAccepted = (code == JOptionPane.YES_OPTION);
					if(callback != null) callback.run();
				} else {
					throw new ClassCastException("Type returned not an Integer");
				}
			}
		});
	}

}
