package net.asmcbain.swing.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;

/**
 * Does exactly what the class-name says it does! Centers a single item in the middle of a
 * container. Adding another item will replace the previous component, be aware. Optionally,
 * a Point2D can be given to indicate an offset from the calculated center (for both x and y). 
 * 
 * @author Art McBain
 *
 */
public class SingleCenteredItemLayout implements LayoutManager2 {

	public static final String CENTER = "center";
	public static final String CENTER_NORTH = "center_north";
	public static final String CENTER_SOUTH = "center_south";
	public static final String CENTER_EAST = "center_east";
	public static final String CENTER_WEST = "center_west";

	protected Map<Container, Constraint> constraints = new HashMap<Container, Constraint>();
	protected final Point2D defaultOffset;

	/**
	 * Constructs a default SingleCenteredItemLayout instance. 
	 */
	public SingleCenteredItemLayout() {
		defaultOffset = null;
	}

	/**
	 * Constructs a SingleCenteredItemLayout instance with the given offset to be used as a
	 * default for new Containers.
	 * 
	 * @param offset from true center
	 */
	public SingleCenteredItemLayout(Point2D offset) {
		defaultOffset = new Point2D.Double(offset.getX(), offset.getY());
	}

	/**
	 * Whether or not the given object is a valid constraint
	 * 
	 * @param constraints the constraints to be checked
	 * @return <code>true</code> if a valid constraint
	 */
	protected boolean isValidConstraint(Object constraints) {
		return !(constraints instanceof String) ||CENTER.equals(constraints) ||
		        CENTER_NORTH.equals(constraints) || CENTER_SOUTH.equals(constraints) ||
		        CENTER_EAST.equals(constraints) || CENTER_WEST.equals(constraints);
	}

	/**
	 * Gets a Constraint object for the given target Container if one already exists,
	 * otherwise a new one is created and associated with the given Container.
	 * 
	 * @param target the target Container for which the Constraint object is to be gotten
	 *               or created
	 * @return a Constraint object for the given target Container
	 */
	protected Constraint getOrCreateConstraint(Container target) {
		if(!constraints.containsKey(target)) {
			constraints.put(target, new Constraint(defaultOffset));
		}
		return constraints.get(target); 
	}

	public void addLayoutComponent(String name, Component constraint) {
		addLayoutComponent(constraint, name);
	}

	// Doesn't get called unless there's a second arg given to container.add(*)
	public void addLayoutComponent(Component comp, Object constraints) {
		if(comp == null || comp.getParent() == null) {
			throw new IllegalArgumentException("Cannot add to layout: neither component nor its parent can be null");
		}

		Container parent = comp.getParent();
		synchronized(parent.getTreeLock()) {

			if(isValidConstraint(constraints)) {
				Constraint constraint = getOrCreateConstraint(parent);
				constraint.component = comp;
				constraint.location = (String)constraints;
			} else {
				throw new IllegalArgumentException("Cannot add to layout: invalid constraint: " + constraints);
			}
		}
	}

	/**
	 * Gets the offset for the given target Container.
	 * 
	 * @param target the Container for which to obtain the offset 
	 * @return the offset associated with the specified Container or <code>null</code> if
	 *         the given Container is not associated with this layout
	 */
	public Point2D getOffset(Container target) {
		synchronized(target.getTreeLock()) {
			Constraint constraint = constraints.get(target);
			return (constraint == null)? null : constraint.offset;
		}
	}

	public void layoutContainer(Container parent) {
		synchronized(parent.getTreeLock()) {
			Constraint constraint = constraints.get(parent);

			if(constraint != null) {
				Insets insets = parent.getInsets();

				Component component = constraint.component;
				double width = component.getPreferredSize().getWidth();
				double height = component.getPreferredSize().getHeight();
				double hOffset = parent.getSize().getWidth() / 2d - width / 2d;
				double vOffset = parent.getSize().getHeight() / 2d - height / 2d;

				if(CENTER_NORTH.equals(constraint.location)) {
					vOffset = insets.top;
				} else if(CENTER_SOUTH.equals(constraint.location)) {
					vOffset = parent.getHeight() - height - insets.bottom;
				} else if(CENTER_EAST.equals(constraint.location)) {
					hOffset = parent.getWidth() - width - insets.right;	
				} else if(CENTER_WEST.equals(constraint.location)) {
					hOffset = insets.left;
				}

				Point2D offset = constraint.offset;
				if(offset != null) {
					hOffset += offset.getX();
					vOffset += offset.getY();
				}

				component.setBounds((int)hOffset, (int)vOffset, (int)width, (int)height);
			}
		}
	}

	public Dimension minimumLayoutSize(Container parent) {
		Dimension dimension = new Dimension();

		synchronized(parent.getTreeLock()) {
			Constraint constraint = constraints.get(parent);

			if(constraint != null) {
				Dimension minSize = constraint.component.getMinimumSize();
				dimension.width = minSize.width;
				dimension.height = minSize.height;

				if(constraint.offset != null) {
					dimension.width += (int)constraint.offset.getX();
					dimension.height += (int)constraint.offset.getY();
				}

				Insets insets = parent.getInsets();
				dimension.width += insets.left + insets.right;
				dimension.height += insets.top + insets.bottom;
			}
		}

		return dimension;
	}

	public Dimension preferredLayoutSize(Container parent) {
		Dimension dimension = new Dimension();

		synchronized(parent.getTreeLock()) {
			Constraint constraint = constraints.get(parent);

			if(constraint != null) {
				Dimension prefSize = constraint.component.getPreferredSize();
				dimension.width = prefSize.width;
				dimension.height = prefSize.height;

				if(constraint.offset != null) {
					dimension.width += (int)constraint.offset.getX();
					dimension.height += (int)constraint.offset.getY();
				}

				Insets insets = parent.getInsets();
				dimension.width += insets.left + insets.right;
				dimension.height += insets.top + insets.bottom;
			}
		}

		return dimension;
	}

	public Dimension maximumLayoutSize(Container parent) {
		Dimension dimension = new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);

		synchronized(parent.getTreeLock()) {
			Constraint constraint = constraints.get(parent);

			if(constraint != null) {
				Dimension maxSize = constraint.component.getMaximumSize();
				Insets insets = parent.getInsets();

				int width = maxSize.width + insets.left + insets.right;
				int height = maxSize.height + insets.top + insets.bottom;

				// Overflow checks
				if(width >= maxSize.width) dimension.width = width;
				if(height >= maxSize.height) dimension.height = height;
			}
		}

		return dimension;
	}

	public void removeLayoutComponent(Component comp) {
		if(comp != null && comp.getParent() != null) {
			constraints.remove(comp.getParent());
		}
	}

	/**
	 * Sets the x and y "offset" from the calculated centered position. For some positions,
	 * either x or y may be ignored. Set to null or Point(0,0) to remove.
	 * 
	 * @param offset the offset from true center to be associated with the given Container
	 */
	public void setOffset(Container target, Point2D offset) {
		Constraint constraint = getOrCreateConstraint(target);
		constraint.offset = new Point2D.Double(offset.getX(), offset.getY());
	}

	public float getLayoutAlignmentX(Container target) {
		return 0f;
	}

	public float getLayoutAlignmentY(Container target) {
		return 0f;
	}

	public void invalidateLayout(Container target) {
		// noop
	}

	/**
	 * Simple class for storing Component -> constraint property mappings
	 * 
	 * @author Art McBain
	 * 
	 */
	protected static class Constraint {

		public Component component;
		public Point2D offset;
		public String location;

		/**
		 * Creates a Constraint instance with the given offset from center
		 * 
		 * @param offset from true center
		 */
		public Constraint(Point2D offset) {
			if(offset != null) {
				this.offset = new Point2D.Double(offset.getX(), offset.getY());
			}
		}

	}

}

