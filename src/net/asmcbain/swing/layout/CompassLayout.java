package net.asmcbain.swing.layout;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

/**
 * Similar to BorderLayout, this layout manager offers position of elements by NSEW
 * constraints. However it also adds constraints NW, NE, SW, SE, EN, ES, WN, WS. Unlike
 * traditional cardinal directions, EN and NE (and the like) are not the same due to the
 * way items are laid out like BorderLayout. This is crudely illustrated below:
 * <br>
 * <pre> +-----------+
 * |<u>NW|  N  |NE</u>|
 * |<u>WN</u>|     |<u>EN</u>|
 * |  |     |  |
 * |W |  C  | E|
 * |__|     |__|
 * |<u>WS</u>|_____|<u>ES</u>|
 * |SW|  S  |SE|
 * +-----------+
 * </pre>
 * @author Art McBain
 *
 */
public class CompassLayout implements LayoutManager2 {

	/** Indicates the area to the top of the container.          */
	public static final String NORTH  = "north";
	/** Indicates the area to the bottom of the container.       */
	public static final String SOUTH  = "south";
	/** Indicates the area to the right of the container.        */
	public static final String EAST   = "east";
	/** Indicates the area to the left of the container.         */
	public static final String WEST   = "west";
	/** Indicates the area to the middle of the container.       */
	public static final String CENTER = "center";
	/** Indicates the area to the top-right of the container.    */
	public static final String NORTH_EAST  = "northeast";
	/** Indicates the area to the top-left of the container.     */
	public static final String NORTH_WEST  = "northwest";
	/** Indicates the area to the lower-right of the container.  */
	public static final String SOUTH_EAST  = "southeast";
	/** Indicates the area to the lower-left of the container.   */
	public static final String SOUTH_WEST  = "southwest";
	/** Indicates the area to the top of the right side area.    */
	public static final String EAST_NORTH  = "eastnorth";
	/** Indicates the area to the bottom of the right side area. */
	public static final String EAST_SOUTH  = "eastsouth";
	/** Indicates the area to the top of the left side area.     */
	public static final String WEST_NORTH  = "westnorth";
	/** Indicates the area to the bottom of the left side area.  */
	public static final String WEST_SOUTH  = "westsouth";


	protected final Map<Container, Map<Object, Component>> components = new HashMap<Container, Map<Object, Component>>();
	protected final Map<Container, Gaps> gaps = new HashMap<Container, Gaps>();
	protected final Gaps defaultGaps;

	/**
	 * Constructs a default CompassLayout instance.
	 */
	public CompassLayout() {
		defaultGaps = null;
	}

	/**
	 * Constructs a CompassLayout instance with the given horizontal and vertical gaps to be
	 * used as defaults for new Containers.
	 * 
	 * @param hgap the horizontal gap
	 * @param vgap the vertical gap
	 */
	public CompassLayout(int hgap, int vgap) {
		defaultGaps = new Gaps(hgap, vgap);
	}

	/**
	 * Whether or not the given object is a valid constraint
	 * 
	 * @param constraints the constraints to be checked
	 * @return <code>true</code> if a valid constraint
	 */
	protected boolean isValidConstraint(Object constraints) {
		return !(constraints instanceof String) || NORTH_WEST.equals(constraints) ||
		       NORTH.equals(constraints) || NORTH_EAST.equals(constraints) ||
		       WEST_NORTH.equals(constraints) || WEST.equals(constraints) ||
		       WEST_SOUTH.equals(constraints) || CENTER.equals(constraints) ||
		       EAST_NORTH.equals(constraints) || EAST.equals(constraints) ||
		       EAST_SOUTH.equals(constraints) || SOUTH_WEST.equals(constraints) ||
		       SOUTH.equals(constraints) || SOUTH_EAST.equals(constraints);
	}

	/**
	 * Gets a Gaps object for the given target Container if one already exists,
	 * otherwise a new one is created and associated with the given Container.
	 * 
	 * @param target the target Container for which the Gaps object is to be gotten
	 *               or created
	 * @return a Gaps object for the given target Container
	 */
	protected Gaps getOrCreateGaps(Container target) {
		if(!gaps.containsKey(target)) {
			gaps.put(target, new Gaps(defaultGaps));
		}
		return gaps.get(target); 
	}

	/**
	 * A "capped" addition method. If the addition of value1 and value2 will result in an
	 * overflow or underflow Integer.MAX_VALUE or Integer.MIN_VALUE will be returned,
	 * respectively. In this way, additions and subtractions are bounded by the maximum
	 * integer values.
	 * 
	 * @param value1 the value to which value2 is added
	 * @param value2 the value added to value1
	 * @return the result of the addition of value1 and value2, Integer.MAX_VALUE if an
	 *         overflow is detected, or Integer.MIN_VALUE if an underflow is detected
	 */
	protected int boundedAdd(int value1, int value2) {
		int temp = value1 + value2;

		if(value1 < 0 || value2 < 0) {
			return (temp > value1)? Integer.MIN_VALUE : temp;
		}

		return (temp < value1)? Integer.MAX_VALUE : temp;
	}

	/**
	 * Translates the given constraint to the appropriate constraint based on the given
	 * component orientation (whether it is left-to-right).
	 * 
	 * @param constraint the constraint to be translated
	 * @param ltr <code>true</code> if left-to-right
	 * @return the translated constraint
	 */
	protected Object getConstraint(Object constraint, boolean ltr) {
		if(ltr) return constraint;

		if(NORTH_EAST.equals(constraint)) return NORTH_WEST;
		if(NORTH_WEST.equals(constraint)) return NORTH_EAST;
		if(SOUTH_WEST.equals(constraint)) return SOUTH_EAST;
		if(SOUTH_EAST.equals(constraint)) return SOUTH_WEST;
		if(WEST.equals(constraint)) return EAST;
		if(WEST_NORTH.equals(constraint)) return EAST_NORTH;
		if(WEST_SOUTH.equals(constraint)) return EAST_SOUTH;
		if(EAST.equals(constraint)) return WEST;
		if(EAST_NORTH.equals(constraint)) return WEST_NORTH;
		if(EAST_SOUTH.equals(constraint)) return WEST_SOUTH;

		return constraint;
	}

	/**
	 * Gets the maximum preferred width between the components at the three given constraints.
	 * Constraints with no associated component are safely ignored. 
	 * 
	 * @param constraints a map of constraints for which the 3 following constraints apply
	 * @param constraint1 the 1st constraint for which the preferred width is to be compared
	 * @param constraint2 the 2nd constraint for which the preferred width is to be compared
	 * @param constraint3 the 3rd constraint for which the preferred width is to be compared
	 * @return the largest preferred width of the components specified by the given
	 *         constraints.
	 */
	protected int maximumPreferredWidth(Map<Object, Component> constraints,
	        Object constraint1, Object constraint2, Object constraint3) {

		Component c1 = constraints.get(constraint1);
		Component c2 = constraints.get(constraint2);
		Component c3 = constraints.get(constraint3);

		int width = 0;
		if(c1 != null) width = Math.max(width, c1.getPreferredSize().width);
		if(c2 != null) width = Math.max(width, c2.getPreferredSize().width);
		if(c3 != null) width = Math.max(width, c3.getPreferredSize().width);

		return width;
	}

	/**
	 * Gets the maximum preferred height between the components at the three given
	 * constraints. Constraints with no associated component are safely ignored. 
	 * 
	 * @param constraints a map of constraints for which the 3 following constraints apply
	 * @param constraint1 the 1st constraint for which the preferred width is to be compared
	 * @param constraint2 the 2nd constraint for which the preferred width is to be compared
	 * @param constraint3 the 3rd constraint for which the preferred width is to be compared
	 * @return the largest maximum preferred height of the components specified by the given
	 *         constraints.
	 */
	protected int maximumPreferredHeight(Map<Object, Component> constraints,
	        Object constraint1, Object constraint2, Object constraint3) {

		Component c1 = constraints.get(constraint1);
		Component c2 = constraints.get(constraint2);
		Component c3 = constraints.get(constraint3);

		int height = 0;
		if(c1 != null) height = Math.max(height, c1.getPreferredSize().height);
		if(c2 != null) height = Math.max(height, c2.getPreferredSize().height);
		if(c3 != null) height = Math.max(height, c3.getPreferredSize().height);

		return height;
	}

	/**
	 * Calculates the sum of the preferred heights of the components at the three given
	 * constraints. Constraints with no associated component are safely ignored. 
	 * 
	 * @param constraints a map of constraints for which the 3 following constraints apply
	 * @param constraint1 the 1st constraint for which the preferred width is to be compared
	 * @param constraint2 the 2nd constraint for which the preferred width is to be compared
	 * @param constraint3 the 3rd constraint for which the preferred width is to be compared
	 * @return the sum of the preferred heights of the components specified by the given
	 *         constraints.
	 */
	protected int sumPreferredHeight(Map<Object, Component> constraints,
	        Object constraint1, Object constraint2, Object constraint3) {

		Component c1 = constraints.get(constraint1);
		Component c2 = constraints.get(constraint2);
		Component c3 = constraints.get(constraint3);

		int height = 0;
		if(c1 != null) height  = c1.getPreferredSize().height;
		if(c2 != null) height += c2.getPreferredSize().height;
		if(c3 != null) height += c3.getPreferredSize().height;

		return height;
	}

	/**
	 * Gets the largest minimum width of the components at the three given constraints.
	 * Constraints with no associated component are safely ignored. 
	 * 
	 * @param constraints a map of constraints for which the 3 following constraints apply
	 * @param constraint1 the 1st constraint for which the preferred width is to be compared
	 * @param constraint2 the 2nd constraint for which the preferred width is to be compared
	 * @param constraint3 the 3rd constraint for which the preferred width is to be compared
	 * @return the largest minimum width of the components specified by the given constraints.
	 */
	protected int maximumMinimumWidth(Map<Object, Component> constraints,
	        Object constraint1, Object constraint2, Object constraint3) {

		Component c1 = constraints.get(constraint1);
		Component c2 = constraints.get(constraint2);
		Component c3 = constraints.get(constraint3);

		int width = 0;
		if(c1 != null) width = Math.max(width, c1.getMinimumSize().width);
		if(c2 != null) width = Math.max(width, c2.getMinimumSize().width);
		if(c3 != null) width = Math.max(width, c3.getMinimumSize().width);

		return width;
	}

	/**
	 * Gets the largest minimum height of the components at the three given constraints.
	 * Constraints with no associated component are safely ignored. 
	 * 
	 * @param constraints a map of constraints for which the 3 following constraints apply
	 * @param constraint1 the 1st constraint for which the preferred width is to be compared
	 * @param constraint2 the 2nd constraint for which the preferred width is to be compared
	 * @param constraint3 the 3rd constraint for which the preferred width is to be compared
	 * @return the largest minimum height of the components specified by the given
	 *         constraints. 
	 */
	protected int maximumMinimumHeight(Map<Object, Component> constraints,
	        Object constraint1, Object constraint2, Object constraint3) {

		Component c1 = constraints.get(constraint1);
		Component c2 = constraints.get(constraint2);
		Component c3 = constraints.get(constraint3);

		int height = 0;
		if(c1 != null) height = Math.max(height, c1.getMinimumSize().height);
		if(c2 != null) height = Math.max(height, c2.getMinimumSize().height);
		if(c3 != null) height = Math.max(height, c3.getMinimumSize().height);

		return height;
	}

	/**
	 * Calculates the sum of the minimum heights of the components at the three given
	 * constraints. Constraints with no associated component are safely ignored. 
	 * 
	 * @param constraints a map of constraints for which the 3 following constraints apply
	 * @param constraint1 the 1st constraint for which the preferred width is to be compared
	 * @param constraint2 the 2nd constraint for which the preferred width is to be compared
	 * @param constraint3 the 3rd constraint for which the preferred width is to be compared
	 * @return the sum of the minimum heights of the components specified by the given
	 *         constraints.
	 */
	protected int sumMinimumHeight(Map<Object, Component> constraints,
	        Object constraint1, Object constraint2, Object constraint3) {

		Component c1 = constraints.get(constraint1);
		Component c2 = constraints.get(constraint2);
		Component c3 = constraints.get(constraint3);

		int height = 0;
		if(c1 != null) height  = c1.getMinimumSize().height;
		if(c2 != null) height += c2.getMinimumSize().height;
		if(c3 != null) height += c3.getMinimumSize().height;

		return height;
	}

	/**
	 * Calculates the total size of all horizontal gaps for the given constraint group.
	 * 
	 * @param constraints a map of constraints for which the following constraint group applies
	 * @param constraint a constraint group for which to calculate the horizontal gap. group
	 *                   must be one of {@link #NORTH}, {@link #CENTER}, and {@link #SOUTH}.
	 * @param gap the Gaps object pertaining to the given map of constraints
	 * @return the total size of the horizontal gap for the given constraint map and group
	 */
	protected int horizontalGap(Map<Object, Component> constraints, Object constraint, Gaps gap) {
		int hgap = 0;

		if(NORTH.equals(constraint)) {

			if(constraints.get(NORTH_WEST) != null && constraints.get(NORTH) != null) {
				hgap += gap.hgap;
			}
			if(constraints.get(NORTH_EAST) != null && constraints.get(NORTH) != null) {
				hgap += gap.hgap;
			}

		} else if(CENTER.equals(constraint)) {

			boolean west = constraints.get(WEST) != null ||
			         constraints.get(WEST_NORTH) != null ||
	                 constraints.get(WEST_SOUTH) != null;

			boolean east = constraints.get(EAST) != null ||
			         constraints.get(EAST_NORTH) != null ||
			         constraints.get(EAST_SOUTH) != null;

			if(west && constraints.get(CENTER) != null) {
				hgap += gap.hgap;
			}
			if(east && constraints.get(CENTER) != null) {
				hgap += gap.hgap;
			}

		} else if(SOUTH.equals(constraint)) {

			if(constraints.get(SOUTH_WEST) != null && constraints.get(SOUTH) != null) {
				hgap += gap.hgap;
			}
			if(constraints.get(SOUTH_EAST) != null && constraints.get(SOUTH) != null) {
				hgap += gap.hgap;
			}

		}

		return hgap;
	}

	/**
	 * Calculates the total size of all vertical gaps for the given constraint group.
	 * 
	 * @param constraints a map of constraints for which the following constraint group applies
	 * @param constraint a constraint group for which to calculate the horizontal gap. group
	 *                   must be one of {@link #WEST}, {@link #CENTER}, and {@link #EAST}.
	 * @param gap the Gaps object pertaining to the given map of constraints
	 * @return the total size of the vertical gap for the given constraint map and group
	 */
	protected int verticalGap(Map<Object, Component> constraints, Object constraint, Gaps gap) {
		int vgap = 0;

		if(WEST.equals(constraint)) {

			if(constraints.get(NORTH_WEST) != null && constraints.get(WEST_NORTH) != null) {
				vgap += gap.vgap;
			}
			if(constraints.get(WEST_NORTH) != null || constraints.get(WEST) != null) {
				vgap += gap.vgap;
			}
			if(constraints.get(WEST_SOUTH) != null && constraints.get(WEST) != null) {
				vgap += gap.vgap;
			}
			if(constraints.get(WEST_SOUTH) != null && constraints.get(SOUTH_WEST) != null) {
				vgap += gap.vgap;
			}

		} else if(CENTER.equals(constraint)) {

			boolean north = constraints.get(NORTH) != null ||
			           constraints.get(NORTH_WEST) != null ||
	                   constraints.get(NORTH_EAST) != null;

			boolean south = constraints.get(SOUTH) != null ||
			           constraints.get(SOUTH_WEST) != null ||
			           constraints.get(SOUTH_EAST) != null;

			if(north && constraints.get(CENTER) != null) {
				vgap += gap.vgap;
			}
			if(south && constraints.get(CENTER) != null) {
				vgap += gap.vgap;
			}

		} else if(EAST.equals(constraint)) {

			if(constraints.get(NORTH_EAST) != null && constraints.get(EAST_NORTH) != null) {
				vgap += gap.vgap;
			}
			if(constraints.get(EAST_NORTH) != null || constraints.get(EAST) != null) {
				vgap += gap.vgap;
			}
			if(constraints.get(EAST_SOUTH) != null && constraints.get(EAST) != null) {
				vgap += gap.vgap;
			}
			if(constraints.get(EAST_SOUTH) != null && constraints.get(SOUTH_EAST) != null) {
				vgap += gap.vgap;
			}

		}

		return vgap;
	}

	public void addLayoutComponent(String name, Component comp) {
		addLayoutComponent(comp, name);
	}

	public void addLayoutComponent(Component comp, Object constraints) {
		if(comp == null || comp.getParent() == null) {
			throw new IllegalArgumentException("Cannot add to layout: neither component nor its parent can be null");
		}
		if(!isValidConstraint(constraints)) {
			throw new IllegalArgumentException("Cannot add to layout: invalid constraint: " + constraints);
		}

		Container parent = comp.getParent();
		synchronized(parent.getTreeLock()) {

			if(components.get(parent) == null) {
				components.put(parent, new HashMap<Object, Component>());
				getOrCreateGaps(parent);
			}

			Map<Object, Component> container = components.get(comp.getParent());
			container.put(constraints, comp);
		}
	}

	/**
	 * Gets the constraints for the given component.
	 * 
	 * @param comp the component for which to get the constraints
	 * @return the constraints for the given component or <code>null</code> if not present
	 *         in this layout
	 */
	public Object getConstraints(Component comp) {
		if(comp == null) return null;

		Container parent = comp.getParent();
		if(parent == null) return null;

		synchronized(parent.getTreeLock()) {
			Map<Object, Component> constraints = components.get(parent);
			Set<Entry<Object, Component>> entries = constraints.entrySet();

			for(Entry<Object, Component> constraint : entries) {
				if(constraint.getValue().equals(comp)) {
					return constraint.getKey();
				}
			}
		}

		return null;
	}

	/**
	 * Gets the horizontal gap for the given target Container.
	 * 
	 * @param target the Container for which the returned horizontal gap applies
	 * @return the horizontal gap for the given container
	 */
	public int getHorizontalGap(Container target) {
		synchronized(target.getTreeLock()) {
			Gaps gap = gaps.get(target);
			return (gap == null)? 0 : gap.hgap;
		}
	}

	/**
	 * Gets the vertical gap for the given target Container.
	 * 
	 * @param target the Container for which the returned vertical gap applies
	 * @return the vertical gap for the given container
	 */
	public int getVerticalGap(Container target) {
		synchronized(target.getTreeLock()) {
			Gaps gap = gaps.get(target);
			return (gap == null)? 0 : gap.vgap;
		}
	}

	/**
	 * Gets the component on the specified target at a location given by the given
	 * constraints.
	 * 
	 * @param target the parent Container to which the given constraints apply
	 * @param constraints the desired location, one of {@link #NORTH_WEST}, {@link #NORTH},
	 *                    {@link #NORTH_EAST}, {@link #WEST_NORTH}, {@link #WEST},
	 *                    {@link #WEST_SOUTH}, {@link #CENTER}, {@link #EAST_NORTH},
	 *                    {@link #EAST}, {@link #EAST_SOUTH}, {@link #SOUTH_WEST},      
	 *                    {@link #SOUTH}, {@link #SOUTH_EAST}.  
	 * @return the component at the given location, or <code>null</code> if the location
	 *         is empty
	 * @exception IllegalArgumentException if the target parameter is null
	 * @exception IllegalArgumentException if the constraint object is not one of the 13
	 *                                     valid constraints
	 */
	public Component getLayoutComponent(Container target, Object constraints) {
		if(target == null) {
			throw new IllegalArgumentException("Cannot get component: target cannot be null");
		}
		if(!isValidConstraint(constraints)) {
			throw new IllegalArgumentException("Cannot get component: invalid constraint: " + constraints);
		}

		synchronized(target.getTreeLock()) {
			return components.get(target).get(constraints);
		}
	}

	/**
	 * Lays out the components for the north or south end of a Container
	 * 
	 * @param constraints map of constraints for the Container being laid out
	 * @param gap the gaps for the Container
	 * @param top the y offset from the top of the Container
	 * @param left the x offset from the top of the Container
	 * @param width the "width" of the the container 
	 * @param height the "height" of the container
	 * @param isNorth true if the container is the north end, false otherwise
	 * @param constraintWest the constraint representing the west item for this end
	 * @param constraintCenter the constraint representing the center item
	 * @param constraintEast the constraint representing the east item
	 * @return the height of the end just laid out
	 */
	protected int layoutEndComponents(Map<Object, Component> constraints, Gaps gap,
	        boolean ltr, int top, int left, int width, int height, boolean isNorth,
	        Object constraintWest, Object constraintCenter, Object constraintEast) {

		Dimension size;
		int x, w;

		Component west = constraints.get(getConstraint(constraintWest, ltr));
		Component center = constraints.get(getConstraint(constraintCenter, ltr));
		Component east = constraints.get(getConstraint(constraintEast, ltr));

		int endHeight = maximumPreferredHeight(constraints,
		        constraintWest, constraintCenter, constraintEast);

		int endOffset = (isNorth)? top : height - endHeight;

		if(west != null) {
			size = west.getPreferredSize();
			west.setBounds(left, endOffset, size.width, endHeight);
		}
		if(east != null) {
			size = east.getPreferredSize();
			east.setBounds(width - size.width, endOffset, size.width, endHeight);
		}
		if(center != null) {
			if(east != null && west != null) {

				x = left + west.getWidth() + gap.hgap;
				w = width - x - east.getPreferredSize().width - gap.hgap;
				center.setBounds(x, endOffset, w, endHeight);

			} else if(east != null) {

				w = width - left - east.getPreferredSize().width - gap.hgap;
				center.setBounds(left, endOffset, w, endHeight);

			} else if(west != null) {

				x = left + west.getPreferredSize().width + gap.hgap;
				w = width - left - west.getPreferredSize().width - gap.hgap;
				center.setBounds(x, endOffset, w, endHeight);

			} else {
				center.setBounds(left, endOffset, width - left, endHeight);
			}
		}

		return endHeight;
	}

	/**
	 * Lays out the components for the west or east end of a Container
	 * 
	 * @param constraints map of constraints for the Container being laid out
	 * @param gap the gaps for the Container
	 * @param top the y offset from the top of the Container
	 * @param left the x offset from the top of the Container
	 * @param width the "width" of the the container 
	 * @param height the "height" of the container
	 * @param isWest true if the container is the west end, false otherwise
	 * @param constraintNorth the constraint representing the west item for this end
	 * @param constraintCenter the constraint representing the center item
	 * @param constraintSouth the constraint representing the east item
	 * @return the width of the end just laid out
	 */
	protected int layoutSideComponents(Map<Object, Component> constraints, Gaps gap,
	        boolean ltr, int top, int left, int width, int height, boolean isWest,
	        Object constraintNorth, Object constraintCenter, Object constraintSouth) {

		Dimension size;
		int y, h;

		Component north = constraints.get(getConstraint(constraintNorth, ltr));
		Component center = constraints.get(getConstraint(constraintCenter, ltr));
		Component south = constraints.get(getConstraint(constraintSouth, ltr));

		int sideWidth = maximumPreferredWidth(constraints,
		        constraintNorth, constraintCenter, constraintSouth);

		int sideOffset = (isWest)? left : width - sideWidth;

		if(north != null) {
			size = north.getPreferredSize();
			north.setBounds(sideOffset, top, sideWidth, size.height);
		}
		if(south != null) {
			size = south.getPreferredSize();
			south.setBounds(sideOffset, height - size.height, sideWidth, size.height);
		}
		if(center != null) {
			if(south != null && north != null) {

				y = top + north.getHeight() + gap.vgap;
				h = height - y - south.getPreferredSize().height - gap.vgap;
				center.setBounds(sideOffset, y, sideWidth, h);

			} else if(south != null) {

				h = height - top - south.getPreferredSize().height - gap.vgap;
				center.setBounds(sideOffset, top, sideWidth, h);

			} else if(north != null) {

				y = top + north.getPreferredSize().height + gap.vgap;
				h = height - top - north.getPreferredSize().height - gap.vgap;
				center.setBounds(sideOffset, y, sideWidth, h);

			} else {
				center.setBounds(sideOffset, top, sideWidth, height - top);
			}
		}

		return sideWidth;
	}

	public void layoutContainer(Container parent) {
		synchronized(parent.getTreeLock()) {
			Map<Object, Component> constraints = components.get(parent);

			if(constraints != null && !constraints.isEmpty()) {

				Gaps gap = gaps.get(parent);
				Insets insets = parent.getInsets();
				boolean ltr = parent.getComponentOrientation().isLeftToRight();

				int top = insets.top;
				int left = insets.left;
				int width = parent.getWidth() - insets.right;
				int height = parent.getHeight() - insets.bottom;

				Component north = constraints.get(NORTH);
				Component south = constraints.get(SOUTH);
				Component northWest = constraints.get(getConstraint(NORTH_WEST, ltr));
				Component northEast = constraints.get(getConstraint(NORTH_EAST, ltr));
				Component southWest = constraints.get(getConstraint(SOUTH_WEST, ltr));
				Component southEast = constraints.get(getConstraint(SOUTH_EAST, ltr));
				Component east = constraints.get(getConstraint(EAST, ltr));
				Component west = constraints.get(getConstraint(WEST, ltr));
				Component eastNorth = constraints.get(getConstraint(EAST_NORTH, ltr));
				Component eastSouth = constraints.get(getConstraint(EAST_SOUTH, ltr));
				Component westNorth = constraints.get(getConstraint(WEST_NORTH, ltr));
				Component westSouth = constraints.get(getConstraint(WEST_SOUTH, ltr));
				Component center = constraints.get(CENTER);

				int nHeight = layoutEndComponents(constraints, gap, ltr, top, left, width,
				        height, true, NORTH_WEST, NORTH, NORTH_EAST);

				if(northWest != null || north != null || northEast != null) {
					nHeight += gap.hgap;
				}

				int sHeight = layoutEndComponents(constraints, gap, ltr, top, left, width,
				        height, false, SOUTH_WEST, SOUTH, SOUTH_EAST);

				if(southWest != null || south != null || southEast != null) {
					sHeight += gap.hgap;
				}

				int eWidth = layoutSideComponents(constraints, gap, ltr, top + nHeight, left,
				        width, height - sHeight, false, EAST_NORTH, EAST, EAST_SOUTH);

				if(eastNorth != null || east != null || eastSouth != null) {
					eWidth += gap.vgap;
				}

				int wWidth = layoutSideComponents(constraints, gap, ltr, top + nHeight, left,
				        width, height - sHeight, true, WEST_NORTH, WEST, WEST_SOUTH);

				if(westNorth != null || west != null || westSouth != null) {
					wWidth += gap.vgap;
				}

				if(center != null) {
					center.setBounds(left + eWidth, top + nHeight,
					        width - eWidth - wWidth - insets.right,
					        height - nHeight - sHeight - insets.bottom);
				}

			}
		}
	}

	public Dimension maximumLayoutSize(Container parent) {
		synchronized(parent.getTreeLock()) {

			Map<Object, Component> constraints = components.get(parent);
			Component north = constraints.get(NORTH);
			Component south = constraints.get(SOUTH);
			Component northWest = constraints.get(NORTH_WEST);
			Component northEast = constraints.get(NORTH_EAST);
			Component southWest = constraints.get(SOUTH_WEST);
			Component southEast = constraints.get(SOUTH_EAST);
			Component east = constraints.get(EAST);
			Component west = constraints.get(WEST);
			Component eastNorth = constraints.get(EAST_NORTH);
			Component eastSouth = constraints.get(EAST_SOUTH);
			Component westNorth = constraints.get(WEST_NORTH);
			Component westSouth = constraints.get(WEST_SOUTH);

			int width = 0;
			int height = 0;

			if(north != null) width = boundedAdd(width, north.getMaximumSize().width);
			if(south != null) width = boundedAdd(width, south.getMaximumSize().width);
			if(northWest != null) width = boundedAdd(width, northWest.getMaximumSize().width);
			if(northEast != null) width = boundedAdd(width, northEast.getMaximumSize().width);
			if(southWest != null) width = boundedAdd(width, southWest.getMaximumSize().width);
			if(southEast != null) width = boundedAdd(width, southEast.getMaximumSize().width);
			if(east != null) height = boundedAdd(height, east.getMaximumSize().height);
			if(west != null) height = boundedAdd(height, west.getMaximumSize().height);
			if(eastNorth != null) height = boundedAdd(height, eastNorth.getMaximumSize().height);
			if(eastSouth != null) height = boundedAdd(height, eastSouth.getMaximumSize().height);
			if(westNorth != null) height = boundedAdd(height, westNorth.getMaximumSize().height);
			if(westSouth != null) height = boundedAdd(height, westSouth.getMaximumSize().height);

			Insets insets = parent.getInsets();
			width = boundedAdd(width, insets.left + insets.right);
			height = boundedAdd(height, insets.top + insets.bottom);

			Dimension dimension = parent.getMaximumSize();
			width = Math.min(dimension.width, width);
			height = Math.min(dimension.height, height);

			return new Dimension(width, height);
		}
	}

	public Dimension preferredLayoutSize(Container parent) {
		synchronized(parent.getTreeLock()) {
			Map<Object, Component> constraints = components.get(parent);

			if(constraints != null) {
				if(constraints.isEmpty()) return new Dimension();

				Component centerComp = constraints.get("CENTER");
				Gaps gap = gaps.get(parent);

				int north = maximumPreferredWidth(constraints, NORTH_WEST, NORTH, NORTH_EAST);
				int south = maximumPreferredWidth(constraints, SOUTH_WEST, SOUTH, SOUTH_EAST);
				int center = (centerComp == null)? 0 : centerComp.getPreferredSize().width;
				int west = maximumPreferredWidth(constraints, WEST_NORTH, WEST, WEST_SOUTH);
				int east = maximumPreferredWidth(constraints, EAST_NORTH, EAST, EAST_SOUTH);
		
				int width = north + horizontalGap(constraints, NORTH, gap);
				width = Math.max(width, south + horizontalGap(constraints, SOUTH, gap));
				width = Math.max(width, west + center + east + horizontalGap(constraints, CENTER, gap));

				north = maximumPreferredHeight(constraints, NORTH_WEST, NORTH, NORTH_EAST);
				south = maximumPreferredHeight(constraints, SOUTH_WEST, SOUTH, SOUTH_EAST);
				center = (centerComp == null)? 0 : centerComp.getPreferredSize().height;
				west = sumPreferredHeight(constraints, WEST_NORTH, WEST, WEST_SOUTH);
				east = sumPreferredHeight(constraints, EAST_NORTH, EAST, EAST_SOUTH);
		
				int height = north + center + south + verticalGap(constraints, CENTER, gap);
				height = Math.max(height, north  + west + south + verticalGap(constraints, WEST, gap));
				height = Math.max(height, north + east + south + verticalGap(constraints, EAST, gap));

				Insets insets = parent.getInsets();
				width += insets.left + insets.right;
				height += insets.top + insets.bottom;

				return new Dimension(width, height);
			}
		}

		return new Dimension();
	}

	public Dimension minimumLayoutSize(Container parent) {
		synchronized(parent.getTreeLock()) {
			Map<Object, Component> constraints = components.get(parent);

			if(constraints != null) {
				if(constraints.isEmpty()) return new Dimension();

				Component centerComp = constraints.get("CENTER");
				Gaps gap = gaps.get(parent);

				int north = maximumMinimumWidth(constraints, NORTH_WEST, NORTH, NORTH_EAST);
				int south = maximumMinimumWidth(constraints, SOUTH_WEST, SOUTH, SOUTH_EAST);
				int center = (centerComp == null)? 0 : centerComp.getPreferredSize().width;
				int west = maximumMinimumWidth(constraints, WEST_NORTH, WEST, WEST_SOUTH);
				int east = maximumMinimumWidth(constraints, EAST_NORTH, EAST, EAST_SOUTH);

				int width = north + horizontalGap(constraints, NORTH, gap);
				width = Math.max(width, south + horizontalGap(constraints, SOUTH, gap));
				width = Math.max(width, west + center + east + horizontalGap(constraints, CENTER, gap));

				north = maximumMinimumHeight(constraints, NORTH_WEST, NORTH, NORTH_EAST);
				south = maximumMinimumHeight(constraints, SOUTH_WEST, SOUTH, SOUTH_EAST);
				center = (centerComp == null)? 0 : centerComp.getPreferredSize().height;
				west = sumMinimumHeight(constraints, WEST_NORTH, WEST, WEST_SOUTH);
				east = sumMinimumHeight(constraints, EAST_NORTH, EAST, EAST_SOUTH);

				int height = north + center + south + verticalGap(constraints, CENTER, gap);
				height = Math.max(height, north  + west + south + verticalGap(constraints, WEST, gap));
				height = Math.max(height, north + east + south + verticalGap(constraints, EAST, gap));

				Insets insets = parent.getInsets();
				width += insets.left + insets.right;
				height += insets.top + insets.bottom;

				return new Dimension(width, height);
			}
		}

		return new Dimension();
	}

	public void removeLayoutComponent(Component comp) {
		if(comp == null) return;

		Container parent = comp.getParent();
		if(components.containsValue(parent)) {

			Map<Object, Component> constraints = components.get(parent);
			Set<Entry<Object, Component>> entries = constraints.entrySet();
			for(Entry<Object, Component> constraint : entries) {

				if(constraint.getValue().equals(comp)) {
					constraints.remove(constraint.getKey());

					// Note: this behavior is depended upon in addLayoutComponent
					//       remember to change the code there if this is modified 
					if(constraints.isEmpty()) {
						components.remove(constraints);
						gaps.remove(parent);
					}
					return;
				}
			}
		}
	}

	/**
	 * Sets the horizontal gap for the given target Container.
	 * 
	 * @param target the Container for which the given horizontal gap applies
	 * @param hgap the horizontal gap
	 */
	public void setHorizontalGap(Container target, int hgap) {
		synchronized(target.getTreeLock()) {
			Gaps gap = getOrCreateGaps(target);
			gap.hgap = hgap;
		}
	}

	/**
	 * Sets the vertical gap for the given target Container.
	 * 
	 * @param target the Container for which the given vertical gap applies
	 * @param vgap the vertical gap
	 */
	public void setVerticalGap(Container target, int vgap) {
		synchronized(target.getTreeLock()) {
			Gaps gap = getOrCreateGaps(target);
			gap.vgap = vgap;
		}
	}

	public float getLayoutAlignmentX(Container target) {
		return 0;
	}
	
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}
	
	public void invalidateLayout(Container target) {
		// noop
	}

	/**
	 * Simple class for storing Container -> hgap/vgap mappings
	 */
	protected static class Gaps {

		public int hgap = 0;
		public int vgap = 0;

		/**
		 * Creates a Gaps instance with the given horizontal and vertical gaps
		 * 
		 * @param hgap horizontal gap
		 * @param vgap vertical gap
		 */
		public Gaps(int hgap, int vgap) {
			this.hgap = hgap;
			this.vgap = vgap;
		}

		/**
		 * Creates a Gaps instance from the given Gaps instance
		 * 
		 * @param gaps instance from which to get the horizontal and vertical
		 *             gaps
		 */
		public Gaps(Gaps gaps) {
			if(gaps != null) {
				hgap = gaps.hgap;
				vgap = gaps.vgap;
			}
		}

	}

}
