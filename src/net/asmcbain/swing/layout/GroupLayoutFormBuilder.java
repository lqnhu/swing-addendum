package net.asmcbain.swing.layout;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;

/**
 * A class designed to wrap GroupLayout instances and provide methods which
 * make it easier to build "two column" forms, with labels on the left, and
 * fields on the right.
 * 
 * @author Art McBain
 *
 */
public class GroupLayoutFormBuilder {

	protected boolean autoCreateGaps;
	protected int hGap;
	protected int vGap;

	protected boolean firstComponent = true;

	protected final GroupLayout layout;

	protected ParallelGroup horizontalGroup;
	protected ParallelGroup horizontalLabelGroup;
	protected ParallelGroup horizontalFieldGroup;

	protected SequentialGroup verticalGroup;

	/**
	 * Creates a GroupLayoutFormBuilder instance that automatically creates
	 * component gaps.
	 * 
	 * @param container for which this builder applies
	 */
	public GroupLayoutFormBuilder(JComponent container) {
		this(container, true);
	}

	/**
	 * Creates a GroupLayoutFormBuilder with the optional ability to create
	 * component gaps.
	 * 
	 * @param container for which this builder applies
	 * @param autoCreateGaps whether or not to auto-create gaps
	 */
	public GroupLayoutFormBuilder(JComponent container, boolean autoCreateGaps) {
		this(container, 0, 0);
		this.autoCreateGaps = autoCreateGaps;
	}

	/**
	 * Creates a GroupLayoutFormBuilder with the given horizontal and vertical
	 * gaps.
	 * 
	 * @param container for which this builder applies
	 * @param hGap horizontal gap
	 * @param vGap vertical gap
	 */
	public GroupLayoutFormBuilder(JComponent container, int hGap, int vGap) {
		this.hGap = hGap;
		this.vGap = vGap;

		layout = new GroupLayout(container);
		container.setLayout(layout);

		if(autoCreateGaps) layout.setAutoCreateGaps(autoCreateGaps);


		horizontalGroup = layout.createParallelGroup(GroupLayout.Alignment.CENTER);
		layout.setHorizontalGroup(horizontalGroup);

		horizontalLabelGroup = layout.createParallelGroup(GroupLayout.Alignment.TRAILING);
		horizontalFieldGroup = layout.createParallelGroup(GroupLayout.Alignment.LEADING);

		SequentialGroup horizontalPairGroup = layout.createSequentialGroup();

		horizontalPairGroup.addGroup(horizontalLabelGroup);
		if(hGap != 0) horizontalPairGroup.addGap(hGap);
		horizontalPairGroup.addGroup(horizontalFieldGroup);

		horizontalGroup.addGroup(horizontalPairGroup);


		verticalGroup = layout.createSequentialGroup();
		layout.setVerticalGroup(verticalGroup);
	}

	/**
	 * Adds a vertical gap.
	 * 
	 * @param size of the gap.
	 */
	public void addVGap(int size) {
		verticalGroup.addGap(size);
	}

	/**
	 * Adds a new label with a spacer instead of a paired field.
	 * 
	 * @param label the text of the label to be added
	 */
	public void addLabel(String label) {
		addLabel(label, true);
	}

	/**
	 * Adds a new label with an optional spacer for the field.
	 * 
	 * @param label the text of the label to be added
	 * @param addSpacerField whether or not to add a spacer field opposite the label 
	 */
	public void addLabel(String label, boolean addSpacerField) {
		addLabel(new JLabel(label), addSpacerField);
	}

	/**
	 * Adds a label with a spacer field.
	 * 
	 * @param label the text of the label to be added
	 */
	public void addLabel(JComponent label) {
		addPair(label, new JPanel());
	}

	/**
	 * Adds a new label with an optional spacer for the field.
	 * 
	 * @param label instance to be added
	 * @param addSpacerField whether or not to add a spacer field opposite the label
	 */
	public void addLabel(JComponent label, boolean addSpacerField) {

		if(addSpacerField) {
			addPair(label, new JPanel());
		} else {

			horizontalGroup.addComponent(label);
			addVerticalComponents(label);
		}
	}

	/**
	 * Adds a field with a blank label.
	 * 
	 * @param field instance to be added
	 */
	public void addField(JComponent field) {
		addPair(new JLabel(), field);
	}

	/**
	 * Adds a field optionally with a blank label.
	 * 
	 * @param field instance to be added
	 * @param addSpacerLabel whether or not to add a spacer label opposite the field
	 */
	public void addField(JComponent field, boolean addSpacerLabel) {

		if(addSpacerLabel) {
			addPair(new JLabel(), field);
		} else {

			horizontalGroup.addComponent(field);
			addVerticalComponents(field);
		}
	}

	/**
	 * Adds a label and its matching field.
	 * 
	 * @param label the text of the label to be added
	 * @param field instance to be added
	 */
	public void addPair(String label, JComponent field) {
		addPair(new JLabel(label), field);
	}

	/**
	 * Adds a label and its matching field.
	 * 
	 * @param label instance to be added
	 * @param field instance to be added
	 */
	public void addPair(JComponent label, JComponent field) {

		horizontalLabelGroup.addComponent(label);
		horizontalFieldGroup.addComponent(field);

		addVerticalComponents(label, field);
	}

	/**
	 * Adds the given components to a vertical group.
	 * 
	 * @param components to be added
	 */
	private void addVerticalComponents(JComponent... components) {

		if(vGap != 0 && !firstComponent) verticalGroup.addGap(vGap);
		firstComponent = false;

		ParallelGroup verticalComponentGroup =
		          layout.createParallelGroup(GroupLayout.Alignment.BASELINE);

		for(JComponent component : components) {
			verticalComponentGroup.addComponent(component);
		}

		verticalGroup.addGroup(verticalComponentGroup);
	}

	/**
	 * Returns the horizontal gap. This will be 0 if this builder was told to
	 * auto-create gaps.
	 * 
	 * @return an int
	 */
	public int getHgap() {
		return hGap;
	}

	/**
	 * Returns the vertical gap. This will be 0 if this builder was told to
	 * auto-create gaps.
	 * 
	 * @return an int
	 */
	public int getVgap() {
		return hGap;
	}

}
