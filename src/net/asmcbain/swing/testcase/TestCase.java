package net.asmcbain.swing.testcase;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Swing test-case template.
 * 
 * @author Art McBain
 *
 */
public class TestCase {

	public static final double RATIO = 1.61803399;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				JFrame frame = new JFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setPreferredSize(new Dimension((int)(500 * RATIO), 500));
				frame.setContentPane(getContentPane());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);

			}
		});
	}

	public static JComponent getContentPane() {
		JPanel panel = new JPanel();

		//

		return panel;
	}

}
