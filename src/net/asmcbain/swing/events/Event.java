package net.asmcbain.swing.events;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a class to aid in event propagation.<br>
 * This class is very simplistic, and doesn't modify any event object
 * information to be appropriate for the component which is now receiving it.
 * This means data such as "x" or "y" locations are relative to the original
 * target, not the layer currently processing it. For comparison, this is how
 * browser events work.
 * <br><br>
 * None of these functions have any boilerplate code, that is, if an event
 * handler throws an error, all processing will be stopped. The error is not
 * caught and hidden.
 * 
 * @author Art McBain
 *
 */
public class Event {

	private static final Integer STOPPED = 1;
	private static final Integer CAPTURING = 2;
	private static final Integer BUBBLING = 4;

	/**
	 * Stores, per-thread, the current phase of event propagation.
	 */
	protected static final ThreadLocal<Integer> phase = new ThreadLocal<Integer>();

	/**
	 * Stores, per-thread, whether or not call on a given thread should
	 * continue propagation of an event.
	 */
	protected static final ThreadLocal<Boolean> propagate = new ThreadLocal<Boolean>();

	/**
	 * Returns the method indicated by the given name on the given component.
	 * 
	 * @param component from which to get the indicated method
	 * @param name of the method to get
	 * @return a Method
	 * @throws NoSuchMethodException if the indicated method name doesn't exist 
	 * @throws SecurityException 
	 */
	protected static Method getMethod(Object component, String name, Class<?>... args) throws SecurityException, NoSuchMethodException {
		Method m = component.getClass().getMethod(name, args);
		// Cheap way to deal with anonymous interfaces, even if it does modify the class
		if(!m.isAccessible()) m.setAccessible(true);
		return m;
	}

	/**
	 * This will "bubble" the event up the component tree, and pass the event
	 * to any item with a listener for the indicated method. Invisible or
	 * disabled items are skipped, as they wouldn't receive any event anyway.
	 * This doesn't take into account the fact that certain elements may or may
	 * not receive an event because of other layering circumstances or
	 * concerns.
	 * <br><br>
	 * It should be noted that it is assumed that the event handler on the
	 * component used to call this method has already processed the event and
	 * determined whether or not the event should be propagated.
	 * <br><br>
	 * To stop propagation, all an event listener has to do is call the
	 * {@link #stopPropagation} method, and all event bubbling will be
	 * stopped, no further components will receive the event.
	 * <br><br>
	 * Propagation will stop naturally when it reaches a window object, as no
	 * further parent elements are user visible. 
	 * @param component The component from which to start the event
	 * propagation.
	 * @param accessorMethodName The name of the method from which to get the
	 * listeners to which to dispatch the event. 
	 * @param listenerMethodName The name of the method on a listener for the
	 * propagating event to which to pass the event.
	 * @param event The actual event object.
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchMethodException if the methods indicated by the given
	 * accessorMethodName or listenerMethodName do not exist
	 * @throws SecurityException 
	 */
	public static void bubble(Component component, String accessorMethodName, String listenerMethodName, Object event) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException {
		phase.set(BUBBLING);
		propagate.set(true);

		Component node = component.getParent();
		Method method = null;

		while(node != null && propagate.get()) {
			method = getMethod(node, accessorMethodName);

			if(node.isVisible() && node.isEnabled()) {

				Object[] listeners = (Object[])method.invoke(node);
				for(Object listener : listeners) {

					getMethod(listener, listenerMethodName, event.getClass())
			                  .invoke(listener, event);
				}
			}
			node = node.getParent();
		}

		phase.set(STOPPED);
		propagate.set(false);
	};

	/**
	 * This will "capture" the event down the DOM tree, and pass the event to
	 * any item with a listener for the indicated method. Invisible or disabled
	 * items are skipped, as they wouldn't receive any event anyway. This
	 * doesn't take into account the fact that certain elements may or may not
	 * receive an event because of other layering circumstances or concerns.
	 * <br><br>
	 * It should be noted that the component used to start the event capturing
	 * process is not called by this process as that could result in recursion.
	 * Instead, it is expected that the event handler for that component will
	 * call this method as its first line, and check the return value for
	 * whether event propagation was stopped (so it knows not to process
	 * anything).
	 * <br><br>
	 * To stop propagation, all an event listener has to do is call the
	 * {@link #stopPropagation} method, and all event capturing will be
	 * stopped, no further components will receive the event. 
	 * @param component The component from which to start the event
	 * propagation.
	 * @param accessorMethodName The name of the method from which to get the
	 * listeners to which to dispatch the event. 
	 * @param listenerMethodName The name of the method on a listener for the
	 * propagating event to which to pass the event.
	 * @param event The actual event object.
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException if the methods indicated by the given
	 * accessorMethodName or listenerMethodName do not exist
	 * @throws NoSuchMethodException 
	 * @throws SecurityException 
	 */
	public static boolean capture(Component component, String accessorMethodName, String listenerMethodName, Object event) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException {
		phase.set(CAPTURING);
		propagate.set(true);

		List<Component> nodes = new ArrayList<Component>();
		Component node = component.getParent();
		Method method = null;

		while(node != null) {
			nodes.add(node);
			node = node.getParent();
		}

		for(int i = nodes.size() - 1; i >= 0 && propagate.get(); i--) {
			method = getMethod(nodes.get(i), accessorMethodName);

			if(nodes.get(i).isVisible() && nodes.get(i).isEnabled()) {

				Object[] listeners = (Object[])method.invoke(nodes.get(i));
				for(Object listener : listeners) {

					getMethod(listener, listenerMethodName, event.getClass())
					        .invoke(listener, event);
				}
			}
		}

		phase.set(STOPPED);
		boolean propagated = propagate.get();
		propagate.set(false);
		return propagated;
	}

	/**
	 * Whether or not an event is in the bubble phase on a given thread. This
	 * {@link #bubble} or {@link #capture} has been called prior to checking
	 * this value.
	 * @return <code>true</code> if the current event is bubbling
	 */
	public static boolean isBubbling() {
		return BUBBLING.equals(phase.get());
	}

	/**
	 * Whether or not an event is in the capture phase on a given thread. This
	 * {@link #bubble} or {@link #capture} has been called prior to checking
	 * this value.
	 * @return <code>true</code> if the current event is capturing
	 */
	public static boolean isCapturing() {
		return CAPTURING.equals(phase.get());
	}

	/**
	 * Whether or not propagation has been stopped for an event on a given
	 * thread. This will only makes sense if the method {@link #bubble} or
	 * {@link #capture} has been called prior to checking this value.
	 * @return <code>true</code> if propagation is stopped for the current
	 * event
	 */
	public static boolean isPropagationStopped() {
		return propagate.get() == null || !propagate.get();
	}

	/**
	 * Propagates the given event by calling both {@link #capture} and
	 * {@link #bubble}. If propagation is not stopped in the capture phase, the
	 * given runnable will be called. If the code inside the runnable does not
	 * stop event propagation, the bubble phase will then begin.
	 * <br><br>
	 * Code which is unsafe, in that it could throw an unlisted exception which
	 * is intended to be caught elsewhere should manually initiate propagation
	 * by directly calling {@link #capture} and {@link #bubble} instead of using
	 * this method. This can be achieved by the following. 
	 * <br><br>
	 * <pre>if(Event.capture(component, accessorMethodName,
	 *           listenerMethodName, event)) {
	 * 
	 *     // code to run if capture propagation is not stopped
	 * 
	 *     if(!propgationShouldBeStopped) {
	 *         Event.bubble(component, accessorMethodName,
	 *                 listenerMethodName, event);
	 *     }
	 * }
	 * </pre>
	 * @param component The component from which to start the event
	 * propagation.
	 * @param accessorMethodName The name of the method from which to get the
	 * listeners to which to dispatch the event. 
	 * @param listenerMethodName The name of the method on a listener for the
	 * propagating event to which to pass the event.
	 * @param event The actual event object.
	 * @return <code>true</code> if the given runnable was called
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws SecurityException
	 * @throws NoSuchMethodException if the methods indicated by the given
	 * accessorMethodName or listenerMethodName do not exist
	 */
	public static boolean propagate(Component component, String accessorMethodName, String listenerMethodName, Object event, Runnable listener) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, SecurityException, NoSuchMethodException {

		if(Event.capture(component, accessorMethodName,
		          listenerMethodName, event)) {

			propagate.set(true);
			listener.run();

			if(!Event.isPropagationStopped()) {
				Event.bubble(component, accessorMethodName,
				          listenerMethodName, event);
			}

			return true;
		}

		return false;
	}

	/**
	 * Stops the propagation of the currently propagating event. This is only
	 * valid for the thread from which it is called.
	 */
	public static void stopPropagation() {
		propagate.set(false);
	}

}
