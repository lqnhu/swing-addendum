package net.asmcbain.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import net.asmcbain.swing.components.table.MutableTableModel;

/**
 * A simple Swing utility class.
 * 
 * @author Art McBain
 *
 */
public class SwingUtil {

	public static final double GOLDEN_RATIO = 1.61803399;

	public static int PADDING_SMALL = 3;
	public static int PADDING_MEDIUM = 7;

	private static final Logger LOGGER = Logger.getLogger(SwingUtil.class.getName());

	/**
	 * Appends the given border to outside of the given component using {@link BorderFactory#createCompoundBorder}
	 * 
	 * @param component to which to append the border
	 * @param border to append
	 */
	public static void appendBorder(JComponent component, Border border) {
		appendBorder(component, border, false);
	}

	/**
	 * Appends the given border to the given component using {@link BorderFactory#createCompoundBorder}
	 * 
	 * @param component to which to append the border
	 * @param border to append
	 * @param inner whether the given border should be appended on the inside
	 */
	public static void appendBorder(JComponent component, Border border, boolean inner) {

		if(component.getBorder() == null) {
			component.setBorder(border);
		} else {
			Border existing = component.getBorder();

			if(inner) {
				existing = BorderFactory.createCompoundBorder(existing, border);
			} else {
				existing = BorderFactory.createCompoundBorder(border, existing);
			}
			component.setBorder(existing);
		}
	}

	/**
	 * Adds a lighter color border shown on focus to the given component. 
	 * 
	 * @param component to be given a focus border
	 */
	public static void createFocusBorder(final JComponent component) {
		if(component.getBorder() == null) return;

		final Border normalBorder = component.getBorder();
		final Border focusBorder = new Border() {
			private RescaleOp colorConvert = new RescaleOp(1.7f, 0, null);

			public Insets getBorderInsets(Component c) {
				return normalBorder.getBorderInsets(c);
			}

			public boolean isBorderOpaque() {
				return normalBorder.isBorderOpaque();
			}

			public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
				BufferedImage bi = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_ARGB);
				normalBorder.paintBorder(c, bi.createGraphics(), x, y, width, height);
			    colorConvert.filter(bi, bi);
				g.drawImage(bi, x, y, x + width, y + height, x, y, x + width, y + height, null);
			}
		};
		component.addFocusListener(new FocusListener() {

			public void focusGained(FocusEvent e) {
				if(!component.getBorder().equals(focusBorder)) {
					component.setBorder(focusBorder);
				}
			}

			public void focusLost(FocusEvent e) {
				component.setBorder(normalBorder);
			}

		});
	}

	/**
	 * Creates a MutableTableModel backed by the given DefaultTableModel.
	 * 
	 * @param model to be wrapped with a MutableTableModel
	 * @return a MutableTableModel instance
	 */
	public static MutableTableModel createMutableTableModel(final DefaultTableModel model) {
		return new MutableTableModel() {

			public void addRow(Object[] rowData) {
				model.addRow(rowData);
			}

			public void addColumn(Object columnName) {
				model.addColumn(columnName);
			}

			public void fireTableRowsUpdated(int firstRow, int lastRow) {
				model.fireTableRowsUpdated(firstRow, lastRow);
			}

			public void removeRow(int row) {
				model.removeRow(row);
			}

			public void setColumnCount(int columnCount) {
				model.setColumnCount(columnCount);
			}

			public void setColumnIdentifiers(Object[] columnIdentifiers) {
				model.setColumnIdentifiers(columnIdentifiers);
			}

			public void setRowCount(int rowCount) {
			}

			public void addTableModelListener(TableModelListener l) {
				model.addTableModelListener(l);
			}

			public Class<?> getColumnClass(int columnIndex) {
				return model.getColumnClass(columnIndex);
			}

			public int getColumnCount() {
				return model.getColumnCount();
			}

			public String getColumnName(int columnIndex) {
				return model.getColumnName(columnIndex);
			}

			public int getRowCount() {
				return model.getRowCount();
			}

			public Object getValueAt(int rowIndex, int colIndex) {
				return model.getValueAt(rowIndex, colIndex);
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return model.isCellEditable(rowIndex, columnIndex);
			}

			public void removeTableModelListener(TableModelListener l) {
				model.removeTableModelListener(l);
			}

			public void setValueAt(Object value, int rowIndex, int columnIndex) {
				model.setValueAt(value, rowIndex, columnIndex);
			}
			
		};
	}

	/**
	 * Creates a form field given by the specified class with a label positioned above it. 
	 * 
	 * @param label the label text
	 * @param clazz the class of the field component
	 * @param parentPanel the parent component to which this pair will be added
	 * @return the requested field component field instance
	 */
	public static JComponent createPairedField(String label, Class<? extends JComponent> clazz, JPanel parentPanel) {

		JPanel pairPanel = new JPanel(new GridLayout(0, 1));
		parentPanel.add(pairPanel);

		JLabel fieldLabel = new JLabel(label);
		fieldLabel.setFont(parentPanel.getFont().deriveFont(Font.BOLD));
		pairPanel.add(fieldLabel);

		try {
			JComponent field = (JComponent)clazz.newInstance();
			pairPanel.add(field);

			return field;
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Unable to create paired field", e);
			return null;
		}
	}

	/**
	 * Creates a form field given by the specified class with a label positioned above
	 * it, ensuring a minimum width given by the specified number of characters.
	 * 
	 * @param label the label text
	 * @param clazz the class of the field component
	 * @param parentPanel the parent component to which this pair will be added
	 * @param example text used to estimate the proper field width
	 * @return the requested field component instance
	 */
	public static JComponent createPairedField(String label, Class<? extends JComponent> clazz, JPanel parentPanel, String example) {
		JComponent field = createPairedField(label, clazz, parentPanel);
		int width = getStringWidth(parentPanel, example) + field.getInsets().left + field.getInsets().right;
		field.setPreferredSize(new Dimension(width, field.getPreferredSize().height));
		return field;
	}

	/**
	 * Creates a panel with a randomly colored background and a default size of 100x25.
	 * 
	 * @return a JPanel instance
	 */
	public static JPanel createPanel() {
		return createPanel(100, 25);
	}

	/**
	 * Creates a panel with a randomly colored background and the given size.
	 * 
	 * @return a JPanel instance
	 */
	public static JPanel createPanel(int width, int height) {

		Color color = Color.getHSBColor(
		          (float)Math.random(), 1f, (float)(Math.random() * .75f + .25f));

		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(width, height));
		panel.setBackground(color);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		return panel;

	}

	/**
	 * Creates a component designed for use as empty space.
	 * 
	 * @param width spacer width
	 * @param height spacer height
	 * @return an empty JComponent
	 */
	public static JComponent createSpacer(int width, int height) {
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(width, height));
		panel.setMinimumSize(panel.getPreferredSize());
		return panel;
	}

	/**
	 * Enables anti-aliasing on an instance of Graphics.
	 * 
	 * @param g graphics on which to force anti-aliasing
	 */
	public static void enableAntiAliasing(Graphics2D g) {
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	}

	/**
	 * Attempts to force the usage of a heavy-weight popup for the given ComboBox.
	 * 
	 * @param comboBox to force having a heavy-weight popup
	 */
	public static void forceHeavyweightPopup(JComboBox comboBox) {
		// The component-taking method is preferred, but this is here because it was
		// written first (keeping compatibility).
		forceHeavyweightPopup((JComponent)comboBox);
		// Setting light-weight popup enabled option doesn't affect anything if its
		// parent has the heavy-weight key set but, again, it's kept for compatibility.
		comboBox.setLightWeightPopupEnabled(false);
	}

	/**
	 * Attempts to force the usage of a heavy-weight popup for any JPopupMenus shown with
	 * the given component as the parent.
	 * 
	 * @param component on which to force heavy-weight popups
	 */
	public static void forceHeavyweightPopup(JComponent component) {
		try {
			Class<?> clazz = Class.forName("javax.swing.PopupFactory");
			Field field = clazz.getDeclaredField("forceHeavyWeightPopupKey");
			field.setAccessible(true);
			component.putClientProperty(field.get(null), Boolean.TRUE);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Unable to force heavyweight popup for component", e);
		}
	}

	/**
	 * Gets the given path as an ImageIcon, useful for getting images that are stored as
	 * files inside a JAR.
	 * 
	 * @param path path to an image file resource
	 * @return an ImageIcon of the image at the given path
	 */
	public static ImageIcon getResourceAsIcon(String path) {
		try {
			return new ImageIcon(ImageIO.read(SwingUtil.class.getResourceAsStream(path)));
		} catch (IOException e) {
			LOGGER.log(Level.WARNING, "Unable to get image resource from path: " + path, e);
		}
		return null;
	}

	/**
	 * Gets the pixel width of a String based on the given component's font.
	 * 
	 * @param component
	 * @param text
	 * @return the pixel width of the supplied String relative to the given component
	 */
	public static int getStringWidth(JComponent component, String text) {
		return SwingUtilities.computeStringWidth(component.getFontMetrics(component.getFont()), text);
	}

	/**
	 * Returns a golden dimension based on the given height.
	 * 
	 * @param height of the dimension
	 * @return a new Dimension based on the golden ration and the given height
	 */
	public static Dimension goldenDimension(int height) {
		return new Dimension((int)(height * GOLDEN_RATIO), height);
	}

	/**
	 * Removes the automatic JTable behavior to advance selection to the next
	 * row when the enter key is pressed.
	 * 
	 * @param table to be fixed
	 */
	public static void removeEnterRowAdvance(JTable table) {
		InputMap inputMap = table.getInputMap(
		          JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
		inputMap.put(enter, new ActionMap());
	}

	/**
	 * Calls {@link #setMaximumHeight(JComponent, int)}.
	 * 
	 * @param component to be adjusted
	 * @param height of the maximum size
	 */
	public static void setMaxHeight(JComponent component, int height) {
		setMaximumHeight(component, height);
	}

	/**
	 * Sets the maximum dimension the given component based on its preferred
	 * width and the supplied height. 
	 * 
	 * @param component to be adjusted
	 * @param height of the maximum size
	 */
	public static void setMaximumHeight(JComponent component, int height) {
		component.setMaximumSize(
		          new Dimension(component.getPreferredSize().width, height));
	}

	/**
	 * Calls {@link #setMaximumWidth(JComponent, int)}.
	 * 
	 * @param component to be adjusted
	 * @param width of the maximum size
	 */
	public static void setMaxWidth(JComponent component, int width) {
		setMaximumWidth(component, width);
	}

	/**
	 * Sets the maximum dimension the given component based on its preferred
	 * height and the supplied width. 
	 * 
	 * @param component to be adjusted
	 * @param width of the maximum size
	 */
	public static void setMaximumWidth(JComponent component, int width) {
		component.setMaximumSize(
		          new Dimension(width, component.getPreferredSize().height));
	}

	/**
	 * Calls {@link #setMinimumHeight(JComponent, int)}.
	 * 
	 * @param component to be adjusted
	 * @param height of the maximum size
	 */
	public static void setMinHeight(JComponent component, int height) {
		setMinimumHeight(component, height);
	}

	/**
	 * Sets the minimum dimension the given component based on its preferred
	 * width and the supplied height. 
	 * 
	 * @param component to be adjusted
	 * @param height of the minimum size
	 */
	public static void setMinimumHeight(JComponent component, int height) {
		component.setMinimumSize(
		          new Dimension(component.getPreferredSize().width, height));
	}

	/**
	 * Calls {@link #setMinimumWidth(JComponent, int)}.
	 * 
	 * @param component to be adjusted
	 * @param width of the minimum size
	 */
	public static void setMinWidth(JComponent component, int width) {
		setMinimumWidth(component, width);
	}

	/**
	 * Sets the minimum dimension the given component based on its preferred
	 * height and the supplied width. 
	 * 
	 * @param component to be adjusted
	 * @param width of the minimum size
	 */
	public static void setMinimumWidth(JComponent component, int width) {
		component.setMinimumSize(
		          new Dimension(width, component.getPreferredSize().height));
	}

	/**
	 * Calls {@link #setPreferredHeight(JComponent, int)}.
	 * 
	 * @param component to be adjusted
	 * @param height of the preferred size
	 */
	public static void setPrefHeight(JComponent component, int height) {
		setPreferredHeight(component, height);
	}

	/**
	 * Sets the preferred dimension the given component based on its preferred
	 * width and the supplied height. 
	 * 
	 * @param component to be adjusted
	 * @param height of the preferred size
	 */
	public static void setPreferredHeight(JComponent component, int height) {
		component.setPreferredSize(
		          new Dimension(component.getPreferredSize().width, height));
	}

	/**
	 * Calls {@link #setPreferredWidth(JComponent, int)}.
	 * 
	 * @param component to be adjusted
	 * @param width of the preferred size
	 */
	public static void setPrefWidth(JComponent component, int width) {
		setPreferredWidth(component, width);
	}

	/**
	 * Sets the preferred dimension the given component based on its preferred
	 * height and the supplied width. 
	 * 
	 * @param component to be adjusted
	 * @param width of the preferred size
	 */
	public static void setPreferredWidth(JComponent component, int width) {
		component.setPreferredSize(
		          new Dimension(width, component.getPreferredSize().height));
	}

	/**
	 * Tries to set the system look and feel. On Linux it attempts to use gtk, and on Mac
	 * it leaves the L&F alone.
	 */
	public static void setSystemLookAndFeel() {
		if(System.getProperty("swing.defaultlaf") == null) {
			if(!System.getProperty("os.name").toLowerCase().contains("mac")) {
				if(System.getProperty("os.name").toLowerCase().contains("windows")) {
					try {
						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					} catch(Exception e) {
						LOGGER.log(Level.INFO, "Failed to set system L&F, using default", e);
					}
				} else {
					try {
						ProcessBuilder builder = new ProcessBuilder("find /usr/lib -name libgtk-x11-*.so");
						Process process = builder.start();
						InputStream is = process.getInputStream();
						if(is != null && is.read() != -1) {
							UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
							is.close();
						}
					} catch(Exception e) {
						LOGGER.log(Level.INFO, "Failed to set GTK L&F, using default", e);
					}
				}
			}
		}
	}

}
