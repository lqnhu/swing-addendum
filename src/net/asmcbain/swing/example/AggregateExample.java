package net.asmcbain.swing.example;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Indicates an example class shows off multiple classes within its package.
 * 
 * @author Art McBain
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface AggregateExample {

}
