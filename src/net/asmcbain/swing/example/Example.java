package net.asmcbain.swing.example;

/**
 * An interface to aid in loading example classes at runtime.
 * 
 * @author Art McBain
 *
 */
public interface Example {

}