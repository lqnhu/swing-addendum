package net.asmcbain.swing.example.components.application;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.asmcbain.swing.components.application.AbstractApplicationWindow;

/**
 * A panel of buttons which initiate duplicate actions, to show how dialogs
 * are queued so they don't appear at the same time, just as if they were
 * real modal dialogs.
 * <br><br>
 * Used by @{link ApplicationExample}.
 * 
 * @author Art McBain
 * 
 */
class DuplicateDialogActionsPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public DuplicateDialogActionsPanel(final AbstractApplicationWindow window) {

		// Show user chosen action
		final JLabel userAction = new JLabel(ApplicationExample.USER_NO_ACT, JLabel.RIGHT);
		userAction.setFont(userAction.getFont().deriveFont(Font.BOLD));

		/*
		 * Show two simple dialogs, a warning followed by an information
		 */
		JButton simpleButton = new JButton("Two Simple Dialogs");
		simpleButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				userAction.setText(ApplicationExample.USER_NO_ACT);

				window.showWarningDialog("Another dialog will appear after this one.");
				window.showInformationDialog("I am the dialog others warned you about.");
			}
		});

		/*
		 * Show an information dialog followed by a question dialog
		 */
		JButton complexButton = new JButton("Two Dialogs (complex)");
		complexButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				window.showInformationDialog("You will be asked a question next.");
				window.showQuestionDialog("Is this a dialog?",

				          // Called after the user has closed the dialog
					      new Runnable() {

				          	public void run() {

				          		// Did the user accept or cancel the dialog?
				          		if(window.wasDialogAccepted()) {
				          			userAction.setText(ApplicationExample.USER_ACCEPT);
				          		} else {
				          			userAction.setText(ApplicationExample.USER_CANCEL);
				          		}
				          }
				});
			}
		});


		// Button layout setup
		JPanel duplicateDialogActionsGrid = new JPanel(new GridLayout(0, 1, 0, 4));
		duplicateDialogActionsGrid.add(simpleButton);
		duplicateDialogActionsGrid.add(complexButton);
		duplicateDialogActionsGrid.add(new JPanel());
		duplicateDialogActionsGrid.add(new JPanel());
		duplicateDialogActionsGrid.add(userAction);

		setLayout(new BorderLayout(0, 2));
		add(new JLabel("<html><b>Duplicate Dialog Actions",
		          JLabel.CENTER), BorderLayout.NORTH);
		add(duplicateDialogActionsGrid, BorderLayout.CENTER);
	}

}
