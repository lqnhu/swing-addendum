package net.asmcbain.swing.example.components.application;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.asmcbain.swing.components.application.AbstractApplicationWindow;

/**
 * A panel of buttons that initiate "simple" dialogs, which consist of just
 * strings that are displayed optionally with an image indicating the type
 * of dialog.
 * <br><br>
 * Used by @{link ApplicationExample}.
 * 
 * @author Art McBain
 * 
 */
class SimpleDialogActionsPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public SimpleDialogActionsPanel(final AbstractApplicationWindow window) {

		/*
		 * Simple error dialog
		 */
		JButton errorButton = new JButton("Error");
		errorButton.addActionListener(new ActionListener() {
	
			public void actionPerformed(ActionEvent e) {
				window.showErrorDialog("An error occurred when connecting to repository:\n"
				          + "Connection to http://svn.example.com/ timed out.");
			}
		});
	
		/*
		 * Simple information dialog
		 */
		JButton infoButton = new JButton("Information");
		infoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	
				String information = "    File:  SomeFile.txt<br>"
				          + "    Size:  4194304 bytes<br>"
				          + "  Folder:  no<br>"
				          + "Readable:  yes<br>"
				          + "Writable:  yes";
				
				window.showInformationDialog("<html><pre>" + information);
			}
		});
	
		/*
		 * Simple plain dialog
		 */
		JButton plainButton = new JButton("Plain");
		plainButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	
				window.showPlainDialog("A simple dialog with no type-indication icon shown.");
			}
		});
	
		/*
		 * Simple warning dialog
		 */
		JButton warningButton = new JButton("Warning");
		warningButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
	
				window.showWarningDialog("An instance of this application is already open.\n"
				          + "Changes to any preferences will not be saved.");
			}
		});
	
	
		// Button layout setup
		JPanel simpleDialogActionsGrid = new JPanel(new GridLayout(0, 1, 0, 4));
		simpleDialogActionsGrid.add(errorButton);
		simpleDialogActionsGrid.add(infoButton);
		simpleDialogActionsGrid.add(plainButton);
		simpleDialogActionsGrid.add(warningButton);
		simpleDialogActionsGrid.add(new JPanel());
	
		setLayout(new BorderLayout(0, 2));
		add(new JLabel("<html><b>Simple Dialog Actions",
		          JLabel.CENTER), BorderLayout.NORTH);
		add(simpleDialogActionsGrid, BorderLayout.CENTER);

	}
}
