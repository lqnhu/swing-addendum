package net.asmcbain.swing.example.components.application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import net.asmcbain.swing.SwingUtil;
import net.asmcbain.swing.components.application.AbstractApplicationWindow;
import net.asmcbain.swing.components.application.ApplicationForm;
import net.asmcbain.swing.layout.GroupLayoutFormBuilder;

/**
 * A panel of of buttons which initiate "complex" dialogs, which can be
 * strings (in the case of question dialogs) or full panels with components
 * and the like.
 * <br><br>
 * Used by @{link ApplicationExample}.
 * 
 * @author Art McBain
 * 
 */
class ComplexDialogActionsPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public ComplexDialogActionsPanel(final AbstractApplicationWindow window) {

		// Show user chosen action
		final JLabel userAction = new JLabel(ApplicationExample.USER_NO_ACT, JLabel.RIGHT);
		userAction.setFont(userAction.getFont().deriveFont(Font.BOLD));

		/*
		 * Show a question dialog
		 */
		JButton questionButton = new JButton("Question");
		questionButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				userAction.setText(ApplicationExample.USER_NO_ACT);

				// Dialog with a "question" icon
				window.showQuestionDialog("There is a new version available of Application X,\n"
				          + "would you like to download the update?",

				          // Called after the user has closed the dialog
				          new Runnable() {

				        	public void run() {

				        		// Did the user accept or cancel the dialog?
				        		if(window.wasDialogAccepted()) {
				        			userAction.setText(ApplicationExample.USER_ACCEPT);
				        		} else {
				        			userAction.setText(ApplicationExample.USER_CANCEL);
				        		}
				        	}
				          });
			}
		});

		JButton panelButton = new JButton("Okay Panel");
		panelButton.addActionListener(new ActionListener() {

			private final JPanel panel = new WelcomeDialog();

			public void actionPerformed(ActionEvent e) {
				userAction.setText(ApplicationExample.USER_NO_ACT);

				// Dialog showing a panel with only an okay button
				window.showPanelDialog(panel);
			}
		});

		/*
		 * Shows a panel based dialog containing a form
		 */
		JButton panelCallbackButton = new JButton("Okay Cancel Panel");
		panelCallbackButton.addActionListener(new ActionListener() {

			private final SimplePanel panel = new SimplePanel();

			public void actionPerformed(ActionEvent e) {
				userAction.setText(ApplicationExample.USER_NO_ACT);

				// Show the pane with a "okay" and "cancel" choices
				window.showPanelDialog(panel, new Runnable() {
					public void run() {

						// If the dialog was accepted, print the user's choices
		        		if(window.wasDialogAccepted()) {
		        			
		        			String product = panel.getProduct();
		        			int quantity = panel.getQuantity();

		        			if(quantity > 1) product += "s";

		        			userAction.setText("Order: " + quantity + " " + product);
		        		} else {
		        			userAction.setText(ApplicationExample.USER_CANCEL);
		        		}
					}
				});
			}
		});

		/*
		 * Show an ApplicationForm-based panel, which makes it easier for
		 * ApplicationWindows to focus the first element on a form and
		 * reset forms if needed.
		 */
		JButton formCallbackButton = new JButton("Okay Cancel Form");
		formCallbackButton.addActionListener(new ActionListener() {

			class SimplePanelForm extends SimplePanel implements ApplicationForm {
				private static final long serialVersionUID = 1L;

				public void focusForm() {
					products.requestFocusInWindow();
				}

				public void resetForm() {
					products.setSelectedIndex(0);
					quantity.setValue(1);
				}

			}

			private final SimplePanelForm panel = new SimplePanelForm();

			public void actionPerformed(ActionEvent e) {
				userAction.setText(ApplicationExample.USER_NO_ACT);

				// Reset form before showing to set it back to defaults 
				panel.resetForm();

				// Show the form with a "okay" and "cancel" choices
				window.showPanelDialog(panel, new Runnable() {
					public void run() {

						// If the dialog was accepted, print the user's choices
		        		if(window.wasDialogAccepted()) {
		        			
		        			String product = panel.getProduct();
		        			int quantity = panel.getQuantity();

		        			if(quantity > 1) product += "s";

		        			userAction.setText("Order: " + quantity + " " + product);
		        		} else {
		        			userAction.setText(ApplicationExample.USER_CANCEL);
		        		}
					}
				});
			}
		});


		// Button layout setup
		JPanel complexDialogActionsGrid = new JPanel(new GridLayout(0, 1, 0, 4));
		complexDialogActionsGrid.add(questionButton);
		complexDialogActionsGrid.add(panelButton);
		complexDialogActionsGrid.add(panelCallbackButton);
		complexDialogActionsGrid.add(formCallbackButton);
		complexDialogActionsGrid.add(userAction);

		setLayout(new BorderLayout(0, 2));
		add(new JLabel("<html><b>Complex Dialog Actions",
		          JLabel.CENTER), BorderLayout.NORTH);
		add(complexDialogActionsGrid, BorderLayout.CENTER);
	}

	/**
	 * A small class that contains a very simple form 
	 * 
	 * @author Art McBain
	 *
	 */
	private static class SimplePanel extends JPanel {
		private static final long serialVersionUID = 1L;

		protected final JComboBox products;
		protected final JSpinner quantity;

		public SimplePanel() {

			products = new JComboBox(new Object[] {
					"Doodad",
					"Gadget",
					"Thingamabob",
					"Thingamajig",
					"Widget"
			});
			products.setEditable(false);

			// Ensure popup appears above the dialog as the normal popup
			// layer is actually below the glass-pane where these dialogs
			// appear
			SwingUtil.forceHeavyweightPopup(products);

			quantity = new JSpinner(new SpinnerNumberModel(1, 1, 25, 1));
			quantity.setValue(1);
			quantity.setMaximumSize(new Dimension(25,
			          quantity.getPreferredSize().height));

			GroupLayoutFormBuilder builder = new GroupLayoutFormBuilder(this, 5, 2);
			builder.addPair("Products", products);
			builder.addPair("Quantity", quantity);					
		}

		public String getProduct() {
			return (String)products.getSelectedItem();
		}

		public int getQuantity() {
			return (Integer)quantity.getValue();
		}

	}

}
