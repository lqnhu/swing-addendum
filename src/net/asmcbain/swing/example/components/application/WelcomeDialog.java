package net.asmcbain.swing.example.components.application;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent.EventType;

/**
 * This dialog is lifted from FileZilla because it makes a great example
 * and that app is awesome. Anyway ... ( http://filezilla-project.org/ )
 * <br><br>
 * Used by @{link ApplicationExample}.
 * 
 * @author Art McBain
 * 
 */
class WelcomeDialog extends JPanel {
	private static final long serialVersionUID = 1L;

	private final Logger LOGGER = Logger.getLogger(WelcomeDialog.class.getName());

	public WelcomeDialog() {

		String exURL = "http://wiki.filezilla-project.org/";
		String list = "<ul style='list-style-type: none; margin: 0 0 0 15px'>";

		// The following isn't pretty, but it's also not the focus of this example
		JEditorPane pane = new JEditorPane();
		pane.setContentType("text/html");
		pane.setEditable(false);
		pane.setText("<html><div style='font-family: sans-serif; font-size: 12pt'>"
		          + "<b>Welcome to FileZilla 3.3.2.1</b><br><hr>"
		          + "<p><b>What's new</b>" + list
		          + "<li><a href='http://filezilla-project.org/versions.php'>new features and improvements in version 5.3.1</a></li></ul></p>"
		          + "<p><b>Documentation</b>" + list
		          + "<li><a href='" + exURL + "Using'>Basic usage instructions</a></li>"
		          + "<li><a href='" + exURL + "Network_Configuration'>Configuring FileZilla</a></li>"
		          + "<li><a href='" + exURL + "Documentation'>Further documentation</a></li></ul></p>"
		          + "<p><b>Getting help</b>" + list
		          + "<li><a href='http://forum.filezilla-project.org/'>Asking questions in the FileZilla forums</a></li>"
		          + "<li><a href='http://filezilla-project.org/support.php#bugs'>Reporting bugs and feature requests</a></li></ul></p>"
		          + "<p>You can always open this dialog again through the help menu.</p><br><hr>");
		pane.setBackground(getBackground());


		final Desktop desktop = Desktop.getDesktop();

		// Show a clicked link in the default browser via the Desktop class,
		// which is only found in Java 6 and later
		pane.addHyperlinkListener(new HyperlinkListener() {

			public void hyperlinkUpdate(HyperlinkEvent e) {

				// Handle only actual clicks (this listener is also notified
				// when the mouse enters or exits a link)
				if(EventType.ACTIVATED.equals(e.getEventType())) {
					try {
						desktop.browse(e.getURL().toURI());
					} catch(Exception e1) {
						LOGGER.log(Level.WARNING, "unable to open URL %s in default browser" + e.getURL(), e1);
					}
				}
			}
		});


		setLayout(new BorderLayout());
		add(pane, BorderLayout.CENTER);
	}

}
