package net.asmcbain.swing.example.layout.singlecentereditemlayout;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import net.asmcbain.swing.SwingUtil;
import net.asmcbain.swing.example.Example;
import net.asmcbain.swing.example.ExampleUtility;
import net.asmcbain.swing.layout.SingleCenteredItemLayout;

/**
 * An example demonstrating the {@link SingleCenteredItemLayout} class.
 * 
 * @author Art McBain
 *
 */
public class SingleCenteredItemLayoutExample implements Example {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtil.setSystemLookAndFeel();

		ExampleUtility.launchExample("SingleCenteredItemLayout Example",
		          getContentPane(), SwingUtil.goldenDimension(500));
	}

	public static JPanel getContentPane() {
		JPanel panel = new JPanel(new GridLayout(0, 2, 2, 2)) {
			private static final long serialVersionUID = 1L;
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);

				// Just to paint a tidy grid between the grid panels
				g.setColor(Color.GRAY);
				g.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight());
				g.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
			}
		};

		SingleCenteredItemLayout layout = new SingleCenteredItemLayout();

		// SCIL with an item centered at the left
		JPanel panel1 = new JPanel(layout);
		panel1.setBorder(BorderFactory.createBevelBorder(0));
		panel1.add(SwingUtil.createPanel(), SingleCenteredItemLayout.CENTER_WEST);
		panel.add(panel1);

		// SCIL with an item centered at the top with an offset of 7 pixels in the y direction
		JPanel panel2 = new JPanel(layout);
		panel2.add(SwingUtil.createPanel(), SingleCenteredItemLayout.CENTER_NORTH);
		panel.add(panel2);
		layout.setOffset(panel2, new Point(0, 7));

		// SCIL with an item centered at the right
		JPanel panel3 = new JPanel(layout);
		panel3.add(SwingUtil.createPanel(), SingleCenteredItemLayout.CENTER_EAST);
		panel.add(panel3);

		// SCIL with an item centered in the middle
		JPanel panel4 = new JPanel(layout);
		panel4.add(SwingUtil.createPanel(), SingleCenteredItemLayout.CENTER);
		panel.add(panel4);

		return panel;
	}

}
