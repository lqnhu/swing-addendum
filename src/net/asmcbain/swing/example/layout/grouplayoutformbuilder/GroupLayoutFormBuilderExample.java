package net.asmcbain.swing.example.layout.grouplayoutformbuilder;

import java.awt.BorderLayout;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.asmcbain.swing.SwingUtil;
import net.asmcbain.swing.example.ExampleUtility;
import net.asmcbain.swing.layout.GroupLayoutFormBuilder;

/**
 * An example demonstrating the {@link GroupLayoutFormBuilder} class.
 * 
 * @author Art McBain
 *
 */
public class GroupLayoutFormBuilderExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtil.setSystemLookAndFeel();

		ExampleUtility.launchExample("GroupLayoutFormBuilder Example",
		          getContentPane());
	}

	public static JPanel getContentPane() {
		JPanel formPanel = new JPanel();

		// Form builder with horizontal gap of 5, and vertical gap of 2
		GroupLayoutFormBuilder builder = new GroupLayoutFormBuilder(formPanel, 5, 2);

		// Labels with blank filler fields
		builder.addLabel("A String label");
		builder.addLabel(new JLabel("<html><b>Custom JLabel"));

		// Labels with no blank filler field
		builder.addLabel("A String label with no filler field", false);
		builder.addLabel(new JLabel("JLabel with no filler field"), false);

		// Field with no associated label
		builder.addField(new JCheckBox("Field with blank filler label"));

		// Field with no blank filler label
		builder.addField(new JCheckBox("Field with no filler label"), false);

		// Label field pairs
		builder.addPair("A String label", new JCheckBox("A field"));
		builder.addPair(new JLabel("A JLabel label"), new JCheckBox("A field"));


		JPanel panel = new JPanel(new BorderLayout(0, 15));
		panel.add(new JLabel("This example is much more useful at the code level"),
		          BorderLayout.NORTH);
		panel.add(formPanel);

		return panel;
	}

}
