package net.asmcbain.events;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * StringEventDispatcher is an extension of EventDispatcher which only accepts String events
 * but enables the setup of whole listeners at a time to be dispatched.
 * 
 * @author Art McBain
 *
 * @param <C> listener type
 */
public class StringEventDispatcher<C> extends EventDispatcher<String, C> implements MultiEventInterface<C> {

	/**
	 * The default naming convention, just returns the given methods' names.
	 */
	public static final NamingConvention DEFAULT_NAMING_CONVENTION = new NamingConvention() {

		public String getEventName(Method method) {

			return method.getName();
		}
	};

	/**
	 * Converts a given methods' names to the appropriate event name by replacing
	 * all uppercase letters with a dash followed by a lowercase letter. A method
	 * name of "run" results in an event name of "run" while a method name of
	 * "doThis" results in an event name of "do-this".
	 */
	public static final NamingConvention DASHED_NAMING_CONVENTION = new NamingConvention() {

		private final Map<String, String> eventNames = new HashMap<String, String>();

		public String getEventName(Method method) {
			String methodName = method.getName();

			if(!methodName.equals(methodName.toLowerCase())) {
				synchronized(eventNames) {
					if(!eventNames.containsKey(methodName)) {

						StringBuilder builder = new StringBuilder(methodName.length());
						for(int i = 0; i < methodName.length(); i++) {
							if(Character.isUpperCase(methodName.charAt(i))) {
								builder.append('-');
								builder.append(Character.toLowerCase(methodName.charAt(i)));
							} else {
								builder.append(methodName.charAt(i));
							}
						}

						eventNames.put(methodName, builder.toString());
					}
					return eventNames.get(methodName);
				}
			}
			return methodName;
		}
	};


	private final NamingConvention convention;

	/**
	 * Creates a default StringEventDispatcher.
	 */
	public StringEventDispatcher() {
		convention = DEFAULT_NAMING_CONVENTION;
	}

	/**
	 * Creates a StringEventDispatcher with the given naming convention.
	 */
	public StringEventDispatcher(NamingConvention convention) {
		this.convention = convention;
	}

	/**
	 * Returns all the events for which a given listener is registered.
	 * 
	 * @param listener
	 * @return
	 */
	public List<String> getEvents(C listener) {
		List<String> events = new ArrayList<String>();

		Method[] methods = listener.getClass().getDeclaredMethods();
		for(Method method : methods) {
			events.add(convention.getEventName(method));
		}

		return events;
	}

	/**
	 * Adds all the methods on the given class to be called on the listeners when a
	 * specific event is dispatched. Event names are determined from method names by
	 * converting all uppercase letters to a dash and a lowercase letter. For example,
	 * a method named "run" would result in an event name of "run", however, a method
	 * named "doStuff" would result in an event name of "do-stuff". 
	 * 
	 * @param clazz
	 */
	public void setDispatchMethods(Class<C> clazz) {
		if(clazz == null) throw new IllegalArgumentException("Class cannot be null");

		Method[] methods = clazz.getDeclaredMethods();
		for(Method method : methods) {
			setDispatchMethod(convention.getEventName(method), method);
		}
	}

	public void addEventListener(C listener) {
		Method[] methods = listener.getClass().getDeclaredMethods();
		synchronized(events) {
			for(Method method : methods) {
				addEventListener(convention.getEventName(method), listener);
			}
		}
	}

	public void removeEventListener(C listener) {
		Method[] methods = listener.getClass().getDeclaredMethods();
		synchronized(events) {
			for(Method method : methods) {
				removeEventListener(convention.getEventName(method), listener);
			}
		}
	};

	/**
	 * Convention for generating an event name for a given method.
	 * 
	 * @author Art McBain
	 *
	 */
	public static interface NamingConvention {

		/**
		 * Gets the event name for a given method.
		 * 
		 * @param method to name
		 * @return a String
		 */
		public String getEventName(Method method);

	}

}
