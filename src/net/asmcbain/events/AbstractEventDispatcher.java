package net.asmcbain.events;

import java.lang.reflect.Method;

/**
 * EventDispatcher is a class designed to help in registering listeners for events and aid in
 * dispatching events.
 * 
 * The first generic argument is the type of the object used to represent objects used to
 * register for events. The second argument is the type of the object being used as a
 * callback, to be notified when an event is dispatched.
 * 
 * Reasons for making this class is there is a small fight between keeping things out of
 * sight from outer code, such as what components are used to build a GUI or other framework.
 * Code on the outside still needs a good way to receive events from those components. To use
 * the GUI example, if an app uses JButtons for click-related events, you'd use an
 * ActionListener. This can then internally be registered directly with the appropriate
 * JButton. However if the app was later changed to use something else such as a JLabel, to
 * continue to use an ActionListener the internals would have to register and intercept a
 * MouseEvent for mouse clicks, then construct an ActionEvent ... which is cumbersome. So why
 * not rewrite the outside to use a MoustListener instead? Well, now we're changing outside
 * code every time we change the internal, which isn't what we want. So instead, make a
 * custom listener (or use an existing one) that accepts the minimum amount of information
 * needed, and have external and internal code work toward that interface. This saves work on
 * outside by not having to rewrite code each time the inside changes (decoupling) and also
 * makes it easier to use your own interfaces to dispatch custom information. Or something
 * like that. :P
 * 
 * @author Art McBain
 *
 * @param <K> event type
 * @param <C> listener type
 */
public abstract class AbstractEventDispatcher<K, C> implements EventInterface<K, C> {

	/**
	 * An exception safe way to look up methods on a given class. Null is returned if the a
	 * method matching the given information cannot be found. 
	 * 
	 * @param clazz
	 * @param method
	 * @param parameters
	 * @return
	 */
	public static Method safeMethodLookup(Class<?> clazz, String method, Class<?>... parameters) {
		try {
			return clazz.getMethod(method, parameters);
		} catch(Exception e) {
			// ignore
		}
		return null;
	}

	/**
	 * Adds the first public method of the specified listener class as the method to which to
	 * be dispatched events. This is useful for classes similar to Runnable which only have
	 * one declared public method.
	 * 
	 * @param event
	 */
	public abstract void setDispatchMethod(K event);

	/**
	 * Adds a method to be called on the listeners when a specific event is dispatched. No
	 * checks can be reasonably done (because of type erasure) so please ensure the given
	 * method is a Method obtained from the specified listener class.
	 * 
	 * @param event
	 * @throws IllegalArgumentException if the given method is null
	 */
	public abstract void setDispatchMethod(K event, Method method);

	/**
	 * Removes the specified dispatch method for the given event. This will cause all
	 * subsequent event dispatches for this event to fail unless a new method to which to
	 * dispatch is set.
	 * 
	 * @param event
	 */
	public abstract void removeDispatchMethod(K event);

	/**
	 * Dispatches the specified event with no parameters to all listeners registered for that
	 * event. If no listeners are registered for a given event, nothing is done. If there is
	 * no registered method to call for this event on the given listener, nothing will be
	 * dispatched.
	 * 
	 * @param event
	 */
	public abstract boolean dispatchEvent(K event);

	/**
	 * Dispatches the specified event with the given parameter values to all listeners
	 * registered for that event. If no listeners are registered for a given event, nothing is
	 * done. If there is no registered method to call for this event on the given listener,
	 * nothing will be dispatched.
	 * 
	 * @param event
	 * @param args
	 * @return
	 */
	public abstract boolean dispatchEvent(K event, Object... args);

}
