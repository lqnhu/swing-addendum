package net.asmcbain.events;

import java.util.List;

/**
 * The first generic argument is the type of the object used to represent objects used to
 * register for events. The second argument is the type of the object being used as a
 * callback, to be notified of an event.
 * 
 * @author Art McBain
 *
 * @param <K> event type
 * @param <C> listener type
 */
public interface EventInterface<K, C> {

	/**
	 * Returns a list of listeners registered for a specific event.
	 * 
	 * @param event
	 * @return
	 */
	public List<C> getEventListeners(K event);

	/**
	 * Registers the given listener for the specified event.
	 * It will be notified when that event is dispatched.
	 * 
	 * @param event
	 * @param listener
	 */
	public void addEventListener(K event, C listener);

	/**
	 * Unregisters the given listener for the specified event.
	 * If no listener is found for that event, nothing is done.
	 * 
	 * @param event
	 * @param listener
	 * @return <code>true</code> if the listener is unregistered, <code>false</code> otherwise
	 */
	public boolean removeEventListener(K event, C listener);

}
